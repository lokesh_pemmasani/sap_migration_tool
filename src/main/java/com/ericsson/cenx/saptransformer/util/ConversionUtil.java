package src.main.java.com.ericsson.cenx.saptransformer.util;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.JarURLConnection;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import javax.swing.JComboBox;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Workbook;

import src.main.java.com.ericsson.cenx.saptransformer.exception.BusinessException;
import src.main.java.com.ericsson.cenx.saptransformer.tools.gui.TransformExcelFilesGui;

public class ConversionUtil {

	static Logger logger = Logger.getLogger(ConversionUtil.class.getName());

	public static void loadPropertiesToComboBox(JComboBox<String> comboBox) {
		Set<String> versionSet = PropertiesCache.getInstance().getAllPropertyNames();

		SortedSet<String> updatedVersionSet = new TreeSet<String>();
		for (String version : versionSet) {
			updatedVersionSet.add(version);
		}

		int existingNumberOfItems = comboBox.getItemCount();
		for (int i = 0; i < existingNumberOfItems; i++) {
			String item = (String) comboBox.getItemAt(i);
			updatedVersionSet.add(item);
		}

		comboBox.removeAllItems();
		for (String version : updatedVersionSet) {
			comboBox.addItem(version);
		}
	}

	public static int determineNumberOfThreadsToUse(File sourceFileDir, int numberOfFiles) {

		int numberOfAvailableProcessors = Runtime.getRuntime().availableProcessors();
		int numberOfThreads = 1;

		if (numberOfFiles <= 3) {
			numberOfThreads = 1;
		} else if (numberOfFiles > 3 && numberOfFiles < 10) {
			numberOfThreads = 2;
		} else {
			int batch = numberOfFiles / 10;
			numberOfThreads = batch + 2;
		}

		if (numberOfAvailableProcessors > numberOfThreads) {
			numberOfThreads = numberOfAvailableProcessors;
		}

		if (numberOfThreads > TransformerConstants.CONST_MAX_NUMBER_OF_THREADS) {
			numberOfThreads = TransformerConstants.CONST_MAX_NUMBER_OF_THREADS;
		}
		return numberOfThreads;
	}

	public static String getTargetTemplateFileName(String inputFileName, String dir) throws URISyntaxException {

		File inputFile = new File(inputFileName);
		String filePath = new File(TransformExcelFilesGui.class.getProtectionDomain().getCodeSource().getLocation().toURI())
				.getAbsolutePath().replace(TransformerConstants.CONST_TRANSFORMER_JAR_NAME, "") + File.separator + "sap-transformer" + File.separator + dir + File.separator 
				+ inputFile.getName();

		return filePath;
	}

	public static List<String> getExemptedSheetList(String exemptedSheets) {
		List<String> exemptedSheetList = null;
		if (!StringUtils.isEmpty(exemptedSheets)) {
			String[] exemptedSheetArray = exemptedSheets.split(",");
			exemptedSheetList = new ArrayList<String>();
			for (String sheet : exemptedSheetArray) {
				exemptedSheetList.add(sheet);
			}
		}
		return exemptedSheetList;
	}

	public static void uploadTemplate(String templateSrcFileString, String targetVersion) throws BusinessException {
		try {
			File targetTemplateFile = new File(ConversionUtil.getTargetTemplateFileName(templateSrcFileString, "target-templates"));
			if (!targetTemplateFile.exists()) {
				// logger.info("Empty file created");
				// targetTemplateFile.createNewFile();
			}

			FileSystem system = FileSystems.getDefault();
			Path original = system.getPath(templateSrcFileString);
			Path target = system.getPath(targetTemplateFile.getAbsolutePath());

			Files.copy(original, target, StandardCopyOption.REPLACE_EXISTING);

			if (StringUtils.isEmpty(targetVersion)) {
				logger.error("Version Cannot be empty");
			} else {
				PropertiesCache.getInstance().updateProperty(targetVersion, targetTemplateFile.getAbsolutePath());
			}
			logger.info("Target Template successfully uploaded");
		} catch (URISyntaxException | IOException e) {
			throw new BusinessException(TransformerConstants.CONST_ERR_MESSAGE_SYSTEM_EXCEPTION + " : " + e.getMessage());
		}
	}

	public static boolean deleteTemplate(String version) {
		boolean status = false;
		PropertiesCache props = PropertiesCache.getInstance();
		if(props.containsKey(version)) {
			String filePath = PropertiesCache.getInstance().getProperty(version);
			File templateFile = new File(filePath);
			if(templateFile.delete()) {
				status = true;
			}
			if(status) {
				props.removeProperty(version);
				
			}
		}
		
		return status;
	}

	public static void reloadComboBox(JComboBox<String> comboBox) {
		
		Set<String> versionSet = PropertiesCache.getInstance().getAllPropertyNames();

		SortedSet<String> updatedVersionSet = new TreeSet<String>();
		for (String version : versionSet) {
			updatedVersionSet.add(version);
		}
		
		comboBox.removeAllItems();
		for (String version : updatedVersionSet) {
			comboBox.addItem(version);
		}
		
	}
	
	public static File getDefaultOutputFolderPath(File input) {
		File tgtDir = new File(FilenameUtils.getFullPath(input.getAbsolutePath()) + "/" + TransformerConstants.CONST_TGT_DEFAULTSUBFOLDER);
		return tgtDir;
	}

	public static void writeTargetFileToDisc(Workbook targetWorkBook, String targetFileName, File dir)
			throws FileNotFoundException, IOException {

		if (!dir.exists()) {
			dir.mkdirs();
		}
		
		String targetFileAbsolutePath = dir.getAbsolutePath() + "/" + targetFileName;
		FileOutputStream targetFileOutputStream = new FileOutputStream(targetFileAbsolutePath);
		targetWorkBook.setActiveSheet(0);
		logger.info("Writing out target file...");
		memoryInfo();
		System.gc();
		targetWorkBook.write(targetFileOutputStream);
		targetFileOutputStream.close();
		targetFileOutputStream = null;
		targetWorkBook.close();
		logger.info("Done! You may already open it: " + targetFileAbsolutePath);
		//memoryInfo();

		logger.info("All Done. The file is available in [" + dir.getAbsolutePath() + "]");
	}
	
	public static void memoryInfo() {

		if (logger.isDebugEnabled()) {
			int mb = 1024 * 1024;

			// Getting the runtime reference from system
			Runtime runtime = Runtime.getRuntime();

			logger.debug("##### Heap utilization statistics [MB] #####");

			logger.debug("Used Memory:" + (runtime.totalMemory() - runtime.freeMemory()) / mb);

			// Print free memory
			logger.debug("Free Memory:" + runtime.freeMemory() / mb);

			// Print total available memory
			logger.debug("Total Memory:" + runtime.totalMemory() / mb);

			// Print Maximum available memory
			logger.debug("Max Memory:" + runtime.maxMemory() / mb);
		}
	}

	public static String getTargetFileName(String sourceFileName, boolean appendVersionInOutput, String targetVersion) {
		String targetFileName = sourceFileName;
		if(appendVersionInOutput) {
			targetFileName = sourceFileName.replace(".xlsx", "_" + targetVersion + ".xlsx");
		}
		return targetFileName;
		
	}
	
	public static void initializeSystem() {
		
		try {
			String filePath = new File(TransformExcelFilesGui.class.getProtectionDomain().getCodeSource().getLocation().toURI()).getAbsolutePath().replace(TransformerConstants.CONST_TRANSFORMER_JAR_NAME, "");
			String transformerDirectoryPath = filePath + File.separator + "sap-transformer";
			File transformerDirectory = new File(transformerDirectoryPath);
			
			if(transformerDirectory.exists()) {
				if(!(new File(getPath(transformerDirectoryPath, "config")).exists())) {
					createSubDirectory(transformerDirectoryPath, "config");
				}
				if(!(new File(getPath(transformerDirectoryPath, "target-templates")).exists())) {
					createSubDirectory(transformerDirectoryPath, "target-templates");
				}
				if(!(new File(getPath(transformerDirectoryPath, "output-reports")).exists())) {
					createSubDirectory(transformerDirectoryPath, "output-reports");
				}
				
			}else {
				transformerDirectory.mkdir();
				createSubDirectory(transformerDirectoryPath, "config");
				createSubDirectory(transformerDirectoryPath, "target-templates");
				createSubDirectory(transformerDirectoryPath, "output-reports");
			}
			
			initializeDefault(filePath);
		
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	private static void initializeDefault(String filePath) {
		Properties initProp = new Properties();
		String initConfigFilePath = filePath + File.separator + "sap-transformer" + File.separator + "config" + File.separator + "init.config";
		File initConfigFile = new File(initConfigFilePath);
		if (!initConfigFile.exists()) {
			try {
				initConfigFile.createNewFile();
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			copyDefaultTemplates(filePath);
			copyDefaultProperties(filePath);
			updateProperties(filePath);
			initProp.put("init-status", "1");
			try {
				OutputStream initWriter = new FileOutputStream(initConfigFilePath);
				initProp.store(initWriter, "Do Not Edit/Delete");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		
	}
	
	private static void updateProperties(String basePath) {
		// TODO Auto-generated method stub
		String targetTemplatePath = basePath + File.separator + "sap-transformer" + File.separator + "target-templates";
		String propertiesFilePath = basePath + File.separator + "sap-transformer" + File.separator + "config" + File.separator + "template-config.properties";
		Properties configProp = new Properties();
		
		File propertiesFile = new File(propertiesFilePath);
		try {
			InputStream fileInputStream = new FileInputStream(propertiesFile);
			configProp.load(fileInputStream);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Map<String, String> fileMap = new HashMap<String, String>();
		
		File templatePath = new File(targetTemplatePath);
		File[] inputFileArray = templatePath.listFiles(new FileExtensionFilter(TransformerConstants.CONST_INPUT_FILE_EXTENSION));
		for (File inputFile: inputFileArray) {
			String fileName = inputFile.getName();
			String filePath = inputFile.getAbsolutePath();
			fileMap.put(fileName, filePath);
		}
		
		Enumeration<Object> keyEnum = configProp.keys();
		while (keyEnum.hasMoreElements()) {
			String key = (String) keyEnum.nextElement();
			String value = configProp.getProperty(key);
			String templateFilePath = fileMap.get(value);
			configProp.put(key, templateFilePath);
			
		}
		
		try {
		OutputStream output = new FileOutputStream(propertiesFilePath);
			configProp.store(output, "Saving default configuration");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	private static void copyDefaultProperties(String filePath) {
		String targetConfigPath = filePath + File.separator + "sap-transformer" + File.separator + "config";
		String sourcePath = "/resources/config";
		init(sourcePath, targetConfigPath);
	}
	
	private static void copyDefaultTemplates(String filePath) {
		String targetTemplatePath = filePath + File.separator + "sap-transformer" + File.separator + "target-templates";
		String sourcePath = "/resources/templates";
		init(sourcePath, targetTemplatePath);
	}

	private static void createSubDirectory(String basePath, String dirName) {
		String dirPath = basePath + File.separator + dirName;
		File dir = new File(dirPath);
		dir.mkdir();
	}
	
	private static String getPath(String basePath, String dirName) {
		String dirPath = basePath + File.separator + dirName;
		return dirPath;
		
	}
	
	private static void init(String sourceDirectory, String writeDirectory) {
		try {
		final URL dirURL = TransformExcelFilesGui.class.getClass().getResource(sourceDirectory);
		final String path = sourceDirectory.substring(1);

		if ((dirURL != null) && dirURL.getProtocol().equals("jar")) {
			final JarURLConnection jarConnection = (JarURLConnection) dirURL.openConnection();

			final ZipFile jar = jarConnection.getJarFile();

			final Enumeration<? extends ZipEntry> entries = jar.entries(); // gives ALL entries in jar

			while (entries.hasMoreElements()) {
				final ZipEntry entry = entries.nextElement();
				final String name = entry.getName();
				if (!name.startsWith(path)) {
					continue;
				}
				final String entryTail = name.substring(path.length());

				final File f = new File(writeDirectory + File.separator + entryTail);
				if (entry.isDirectory()) {
					// if its a directory, create it
					final boolean bMade = f.mkdir();
					//System.out.println((bMade ? "  creating " : "  unable to create ") + name);
				} else {
					final InputStream is = jar.getInputStream(entry);
					final OutputStream os = new BufferedOutputStream(new FileOutputStream(f));
					final byte buffer[] = new byte[4096];
					int readCount;
					// write contents of 'is' to 'os'
					while ((readCount = is.read(buffer)) > 0) {
						os.write(buffer, 0, readCount);
					}
					os.close();
					is.close();
				}
			}

		} else if (dirURL == null) {
			
			System.out.println("can't find " + sourceDirectory + " on the classpath");
		} else {
			System.out.println("don't know how to handle extracting from " + dirURL);
		}
		}catch(Exception e) {
			System.out.println("Exception");
			e.printStackTrace();
		}
	}

}
