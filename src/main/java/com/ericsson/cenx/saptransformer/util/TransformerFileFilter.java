package src.main.java.com.ericsson.cenx.saptransformer.util;

import java.io.File;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import src.main.java.com.ericsson.cenx.saptransformer.exception.BusinessException;
import src.main.java.com.ericsson.cenx.saptransformer.stats.ErrorCatalog;
import src.main.java.com.ericsson.cenx.saptransformer.stats.ReportManager;

public class TransformerFileFilter {
	
	private static Logger logger = Logger.getLogger(TransformerFileFilter.class.getName());
	
	public static List<File> findFiles(File inputDirectory) throws BusinessException{
		ReportManager reportManager = ReportManager.getInstance();
		List<File> inputFileList = null;
		
		if (!inputDirectory.exists()) {
			logger.error("Input Directory [" + inputDirectory.getAbsolutePath() + "] doesn't exists");
			throw new BusinessException("Input Directory [" + inputDirectory.getAbsolutePath() + "] doesn't exists");
		}
			
		File[] inputFileArray = inputDirectory.listFiles(new FileExtensionFilter(TransformerConstants.CONST_INPUT_FILE_EXTENSION));
		
		if (inputFileArray.length == 0) {
			logger.error("Input Directory [" + inputDirectory.getAbsolutePath() + "] doesn't contain any valid file.");
			ErrorCatalog errorCatalog = new ErrorCatalog();
			errorCatalog.setErrorCode(TransformerConstants.CONST_ERR_CODE_INPUTDIREMPTY);
			errorCatalog.setErrorMessage(MessageFormat.format(TransformerConstants.CONST_ERR_MESSAGE_INPUTDIREMPTY, inputDirectory.getAbsolutePath()));
			reportManager.setValidationError(true);
			reportManager.setValidationFileErrorCatalog(errorCatalog);
			throw new BusinessException(MessageFormat.format(TransformerConstants.CONST_ERR_MESSAGE_INPUTDIREMPTY, inputDirectory.getAbsolutePath()));
		
		} else {
			inputFileList = new ArrayList<File>();
			for (File inputFile: inputFileArray)
				inputFileList.add(inputFile);
		}
		return inputFileList;
	}
	
}
