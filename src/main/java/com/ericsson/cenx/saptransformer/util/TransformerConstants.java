package src.main.java.com.ericsson.cenx.saptransformer.util;

import java.io.File;
import java.util.Date;

public interface TransformerConstants {
	
	public static final String CONST_TARGET_VERSION_FILE = "config/template-config.properties";
	public static final int CONST_MAX_NUMBER_OF_THREADS = 50;
	public static final String CONST_TRANSFORMER_JAR_NAME = File.separator + "sap-transformer.jar";
	public static final String CONST_TGT_DEFAULTSUBFOLDER = "transformer_output/" + Long.toString((new Date()).getTime());
	public static final String CONST_INPUT_FILE_EXTENSION = ".xlsx";
	
	public static final String CONST_ERR_CODE_FILENOTFOUND = "VAL-E001";
	public static final String CONST_ERR_CODE_INPUTDIRNOTFOUND = "VAL-E002";
	public static final String CONST_ERR_CODE_OUTPUTDIRNOTFOUND = "VAL-E003";
	public static final String CONST_ERR_CODE_INPUTOUTPUTDIREQUAL = "VAL-E004";
	public static final String CONST_ERR_CODE_INPUTDIREMPTY = "VAL-E005";
	public static final String CONST_ERR_CODE_INPUTFILEINVALID = "VAL-E006";
	public static final String CONST_ERR_CODE_INPUTFILEENCRYPED = "VAL-E007";
	public static final String CONST_ERR_CODE_TARGETTEMPLATENOTFOUND = "VAL-E008";
	public static final String CONST_ERR_CODE_TARGETTEMPLATEINVALID = "VAL-E009";
	public static final String CONST_ERR_CODE_TARGETTEMPLATEENCRYPTED = "VAL-E010";
	
	public static final String CONST_ERR_CODE_UNKNOWNSYSTEMEXCEPTION = "SYS-E001";
	public static final String CONST_ERR_CODE_UPLOADTEMPLATEVERSIONEXISTS = "SYS-E002";
	public static final String CONST_ERR_CODE_UPLOADTEMPLATEFILEEXISTSINCONF = "SYS-E003";
	public static final String CONST_ERR_CODE_UPLOADTEMPLATEFILEDOESNOTEXISTS = "SYS-E004";
	public static final String CONST_ERR_CODE_UPLOADTEMPLATEFILEALREADYDELETED = "SYS-E005";
	public static final String CONST_ERR_CODE_UPLOADTEMPLATEFILEENCRYPTED = "SYS-E006";
	public static final String CONST_ERR_CODE_UPLOADTEMPLATEFILEINVALID = "SYS-E007";
	
	public static final String CONST_ERR_MESSAGE_FILENOTFOUND = "Input file not found in specified location [{0}]";
	public static final String CONST_ERR_MESSAGE_INPUTDIRNOTFOUND = "Input directory does not exist [{0}]";
	public static final String CONST_ERR_MESSAGE_OUTPUTDIRNOTFOUND = "Output directory does not exist [{0}]";
	public static final String CONST_ERR_MESSAGE_INPUTOUTPUTDIREQUAL = "Input directory and Output directory are same [{0}]";
	public static final String CONST_ERR_MESSAGE_INPUTDIREMPTY = "Input directory does not contain any .xlsx file [{0}]";
	public static final String CONST_ERR_MESSAGE_INPUTFILEINVALID = "Invalid Format of Source file [{0}]";
	public static final String CONST_ERR_MESSAGE_INPUTFILEENCRYPED = "Source File is encrypted [{0}]";
	public static final String CONST_ERR_MESSAGE_TARGETTEMPLATENOTFOUND = "Target template file not found in location [{0}]";
	public static final String CONST_ERR_MESSAGE_TARGETTEMPLATEINVALID = "Invalid format of Target Template [{0}]";
	public static final String CONST_ERR_MESSAGE_TARGETTEMPLATEENCRYPTED = "Target Template is encrypted [{0}]";
	
	
	public static final String CONST_ERR_MESSAGE_UNKNOWNSYSTEMEXCEPTION = "Unknown System exception for file [{0}]";
	public static final String CONST_ERR_MESSAGE_UPLOADTEMPLATEVERSIONEXISTS = "Target Template version already exists [{0}]";
	public static final String CONST_ERR_MESSAGE_UPLOADTEMPLATEFILEEXISTSINCONF = "Target template file already exists [{0}]";
	public static final String CONST_ERR_MESSAGE_UPLOADTEMPLATEFILEDOESNOTEXISTS = "Target template file does not exists in specified source [{0}]";
	public static final String CONST_ERR_MESSAGE_UPLOADTEMPLATEFILEALREADYDELETED = "Template file already deleted [{0}]";
	public static final String CONST_ERR_MESSAGE_UPLOADTEMPLATEFILEENCRYPTED = "Template file encrypted [{0}]";
	public static final String CONST_ERR_MESSAGE_UPLOADTEMPLATEFILEINVALID = "Template file invalid format [{0}]";
	
	public static final String CONST_ERR_MESSAGE_SYSTEM_EXCEPTION = "Unknown System Exception";
	
	
}
