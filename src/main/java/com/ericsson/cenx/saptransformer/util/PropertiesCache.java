package src.main.java.com.ericsson.cenx.saptransformer.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URISyntaxException;
import java.util.Enumeration;
import java.util.Properties;
import java.util.Set;

import org.apache.log4j.Logger;

public class PropertiesCache {

	static Logger logger = Logger.getLogger(PropertiesCache.class.getName());

	private static final Properties configProp = new Properties();
	private String propertiesFilePath = null;

	private PropertiesCache() {
		// Private constructor to restrict new instances
		try {
			ConversionUtil.initializeSystem();
			propertiesFilePath = ConversionUtil.getTargetTemplateFileName("template-config.properties", "config");
			// propertiesFilePath = new
			// File(PropertiesCache.class.getProtectionDomain().getCodeSource().getLocation().toURI()).getAbsolutePath().replace(File.separator
			// + "sap-transformer.jar", "") + File.separator + "template-config.properties";
			File propertiesFile = new File(propertiesFilePath);
			if (propertiesFile.exists()) {
				InputStream fileInputStream = new FileInputStream(propertiesFile);
				configProp.load(fileInputStream);
			} else {
				propertiesFile.createNewFile();
			}

		} catch (URISyntaxException e1) {
			e1.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("Read all properties from file");

	}

	private static class LazyHolder {
		private static final PropertiesCache INSTANCE = new PropertiesCache();
	}

	public static PropertiesCache getInstance() {
		return LazyHolder.INSTANCE;
	}

	public String getProperty(String key) {
		return configProp.getProperty(key);
	}

	public Set<String> getAllPropertyNames() {
		return configProp.stringPropertyNames();
	}
	
	public Enumeration<Object> getAllKeys() {
		return configProp.keys();
	}

	public boolean containsKey(String key) {
		return configProp.containsKey(key);
	}

	public void updateProperty(String key, String value) {
		configProp.put(key, value);
		try {
			OutputStream output = new FileOutputStream(propertiesFilePath);
			configProp.store(output, null);
			reloadConfiguration();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void removeProperty(String key) {
		configProp.remove(key);
		try {
			OutputStream output = new FileOutputStream(propertiesFilePath);
			configProp.store(output, null);
			reloadConfiguration();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void reloadConfiguration() throws IOException {
		File propertiesFile = new File(propertiesFilePath);
		InputStream fileInputStream = new FileInputStream(propertiesFile);
		configProp.load(fileInputStream);
	}

}
