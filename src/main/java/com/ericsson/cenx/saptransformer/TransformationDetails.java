package src.main.java.com.ericsson.cenx.saptransformer;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class TransformationDetails {
	
	private File sourceDirectory;
	private File sourceFile;
	private File targetFile;
	private File outputDir;
	private List<String> exemptedSheetList;
	private boolean skipHiddenSheets;
	private boolean appendVersionInOutput;
	private String targetVersion;
	private boolean bulkOperation;
	
	public TransformationDetails(){
		this.exemptedSheetList = new ArrayList<String>();
	}
	
	public File getSourceDirectory() {
		return sourceDirectory;
	}
	public void setSourceDirectory(File sourceDirectory) {
		this.sourceDirectory = sourceDirectory;
	}
	public File getSourceFile() {
		return sourceFile;
	}
	public void setSourceFile(File sourceFile) {
		this.sourceFile = sourceFile;
	}
	public File getTargetFile() {
		return targetFile;
	}
	public void setTargetFile(File targetFile) {
		this.targetFile = targetFile;
	}
	public File getOutputDir() {
		return outputDir;
	}
	public void setOutputDir(File outputDir) {
		this.outputDir = outputDir;
	}
	public List<String> getExemptedSheetList() {
		return exemptedSheetList;
	}
	public void setExemptedSheetList(List<String> exemptedSheetList) {
		this.exemptedSheetList.addAll(exemptedSheetList);
	}
	public void adddSheetToExemptedList(String exemptedSheet) {
		this.exemptedSheetList.add(exemptedSheet);
	}
	public boolean isSkipHiddenSheets() {
		return skipHiddenSheets;
	}
	public void setSkipHiddenSheets(boolean skipHiddenSheets) {
		this.skipHiddenSheets = skipHiddenSheets;
	}
	public boolean isAppendVersionInOutput() {
		return appendVersionInOutput;
	}
	public void setAppendVersionInOutput(boolean appendVersionInOutput) {
		this.appendVersionInOutput = appendVersionInOutput;
	}
	public String getTargetVersion() {
		return targetVersion;
	}
	public void setTargetVersion(String targetVersion) {
		this.targetVersion = targetVersion;
	}
	public boolean isBulkOperation() {
		return bulkOperation;
	}
	public void setBulkOperation(boolean bulkOperation) {
		this.bulkOperation = bulkOperation;
	}

	public void copy(TransformationDetails object) {
		this.sourceFile = object.getSourceFile();
		this.targetFile = object.getTargetFile();
		this.outputDir = object.getOutputDir();
		this.exemptedSheetList = object.getExemptedSheetList();
		this.skipHiddenSheets = object.isSkipHiddenSheets();
		this.appendVersionInOutput = object.isAppendVersionInOutput();
		this.targetVersion = object.getTargetVersion();
	}
	
}
