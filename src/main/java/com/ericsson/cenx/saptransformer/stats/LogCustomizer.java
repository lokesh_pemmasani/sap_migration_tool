package src.main.java.com.ericsson.cenx.saptransformer.stats;

import java.util.Enumeration;

import org.apache.log4j.Category;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class LogCustomizer {
	
	static Logger logger = Logger.getLogger(LogCustomizer.class.getName());

	public static final void changeLogLevel(boolean isDebugEnabled) {
		Logger root = Logger.getRootLogger();
		Enumeration<Category> allLoggers = root.getLoggerRepository().getCurrentCategories();

		if (isDebugEnabled) {
			root.setLevel(Level.DEBUG);
			while (allLoggers.hasMoreElements()) {
				Category tmpLogger = (Category) allLoggers.nextElement();
				tmpLogger.setLevel(Level.DEBUG);
			}
		} else {
			root.setLevel(Level.INFO);
			while (allLoggers.hasMoreElements()) {
				Category tmpLogger = (Category) allLoggers.nextElement();
				tmpLogger.setLevel(Level.INFO);
			}
		}
	}

	public static final void switchLogging(boolean loggingStatus) {

		Logger root = Logger.getRootLogger();
		Enumeration<Category> allLoggers = root.getLoggerRepository().getCurrentCategories();

		if (loggingStatus) {
			logger.info("Turning the logger off");
			root.setLevel(Level.OFF);
			while (allLoggers.hasMoreElements()) {
				Category tmpLogger = (Category) allLoggers.nextElement();
				tmpLogger.setLevel(Level.OFF);
			}
		} else {
			logger.info("Turning the logger on");
			root.setLevel(Level.INFO);
			while (allLoggers.hasMoreElements()) {
				Category tmpLogger = (Category) allLoggers.nextElement();
				tmpLogger.setLevel(Level.INFO);
			}
		}
	}

}
