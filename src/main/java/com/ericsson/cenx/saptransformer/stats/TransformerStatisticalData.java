package src.main.java.com.ericsson.cenx.saptransformer.stats;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class TransformerStatisticalData {
	
	private String inputFile;
	private String outputFile;
	private String outputFileDir;
	private int numOfTabsInSource;
	private List<String> hiddenSheetList;
	private List<String> missingSheetList;
	private Map<String, List<String>> missingColumnList;
	private Date startTime;
	private Date endTime;
	
	private boolean hasError;

	private ErrorCatalog errorCatalog;
	
	public TransformerStatisticalData() {
		this.missingSheetList = new ArrayList<String>();
		this.hiddenSheetList = new ArrayList<String>();
		this.missingColumnList = new LinkedHashMap<String, List<String>>();
	}
	
	public Date getStartTime() {
		return this.startTime;
	}
	
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}
	
	public Date getEndTime() {
		return this.endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
	
	public int getNumOfTabsInSource() {
		return numOfTabsInSource;
	}

	public void setNumOfTabsInSource(int numOfTabsInSource) {
		this.numOfTabsInSource = numOfTabsInSource;
	}
	
	public String getInputFile() {
		return inputFile;
	}
	
	public void setInputFile(String inputFile) {
		this.inputFile = inputFile;
	}
	
	public String getOutputFile() {
		return outputFile;
	}

	public void setOutputFile(String outputFile) {
		this.outputFile = outputFile;
	}
	
	public String getOutputFileDir() {
		return outputFileDir;
	}

	public void setOutputFileDir(String outputFileDir) {
		this.outputFileDir = outputFileDir;
	}

	public void addSheetToMissingList(String sheetName){
		missingSheetList.add(sheetName);
	}
	
	public List<String> getMissingSheetList(){
		return this.missingSheetList;
	}
	
	public void setMissingSheetList(List<String> missingSheetList) {
		this.missingSheetList.addAll(missingSheetList);
	}

	public void addSheetToHiddenSheetList(String sheetName){
		hiddenSheetList.add(sheetName);
	}
	
	public List<String> getHiddenSheetList(){
		return this.hiddenSheetList;
	}
	
	public void setHiddenSheetList(List<String> hiddenSheetList) {
		this.hiddenSheetList.addAll(hiddenSheetList);
	}
	
	public void addColumnToMissingList(String sheetName, String columnName){
		if(this.missingColumnList.get(sheetName) == null) {
			missingColumnList.put(sheetName, new ArrayList<String>());
		}
		missingColumnList.get(sheetName).add(columnName);
	}
	
	public Map<String, List<String>> getAllMissingColumnList(){
		return this.missingColumnList;
	}
	
	public List<String> getMissingColumnList(String sheetName){
		return this.missingColumnList.get(sheetName);
	}

	public Map<String, List<String>> getMissingColumnList() {
		return missingColumnList;
	}

	public void setMissingColumnList(Map<String, List<String>> missingColumnList) {
		this.missingColumnList = missingColumnList;
	}
	
	public boolean hasError() {
		return hasError;
	}

	public void setHasError(boolean hasError) {
		this.hasError = hasError;
	}

	public ErrorCatalog getErrorCatalog() {
		return errorCatalog;
	}

	public void setErrorCatalog(ErrorCatalog errorCatalog) {
		this.errorCatalog = errorCatalog;
	}

}
