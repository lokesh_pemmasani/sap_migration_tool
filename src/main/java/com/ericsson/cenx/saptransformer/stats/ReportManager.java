package src.main.java.com.ericsson.cenx.saptransformer.stats;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;

import src.main.java.com.ericsson.cenx.saptransformer.exception.BusinessException;
import src.main.java.com.ericsson.cenx.saptransformer.util.ConversionUtil;

public class ReportManager {

	static Logger logger = Logger.getLogger(ReportManager.class.getName());
	private static ReportManager instance;

	private String reportName;
	private boolean bulkProcessing;
	private int totalFilesAvailable;
	private List<File> listOfFilesToProcess;
	private int totalSuccessFiles; 
	private int totalFailedFiles; 
	private int totalNumberOfThreadsUsed; 
	private Date startTime;
	private Date endTime;
	private String inputDirectory;
	private String outputDirectory;
	private boolean validationError; 
	private ErrorCatalog validationFileErrorCatalog; 
	
	private List<TransformerStatisticalData> statList; 
	private List<String> exemptedSheetList;
	private boolean skipHiddenSheets;
	private String targetVersion;
	
	

	private ReportManager() {
		logger.info("Initializing report manager");
		this.bulkProcessing = false;
		this.skipHiddenSheets = false;
		this.totalFilesAvailable = 0;
		this.validationError = false;
		this.totalSuccessFiles = 0;
		this.totalFailedFiles = 0;
		this.totalNumberOfThreadsUsed = 0;
		this.statList = new ArrayList<TransformerStatisticalData>();
		this.listOfFilesToProcess = new ArrayList<File>();
		this.exemptedSheetList = new ArrayList<String>();
		this.validationFileErrorCatalog = new ErrorCatalog();
	}

	public static ReportManager getInstance() {
		if (instance == null) {
			instance = new ReportManager();
		}
		return instance;
	}

	public void resetReportManager() {
		this.reportName = null;
		this.bulkProcessing = false;
		this.skipHiddenSheets = false;
		this.totalFilesAvailable = 0;
		this.validationError = false;
		this.totalSuccessFiles = 0;
		this.totalFailedFiles = 0;
		this.totalNumberOfThreadsUsed = 0;
		this.startTime = null;
		this.endTime = null;
		this.inputDirectory = null;
		this.outputDirectory = null;
		this.targetVersion = null;
		this.statList = new ArrayList<TransformerStatisticalData>();
		this.listOfFilesToProcess = new ArrayList<File>();
		this.exemptedSheetList = new ArrayList<String>();
		this.validationFileErrorCatalog = null;
	}
	
	public void setTargetVersion(String targetVersion) {
		this.targetVersion = targetVersion;
	}

	public void setBulkProcessing(boolean bulkProcessing) {
		this.bulkProcessing = bulkProcessing;
	}

	public void incrementTotalSuccessFiles() {
		this.totalSuccessFiles++;
	}

	public void incrementTotalFailedFiles() {
		this.totalFailedFiles++;
	}
	
	public void setInputDirectory(String inputDirectory) {
		this.inputDirectory = inputDirectory;
	}

	public void setOutputDirectory(String outputDirectory) {
		this.outputDirectory = outputDirectory;
	}

	public boolean isValidationError() {
		return validationError;
	}

	public void setValidationError(boolean targetFileError) {
		this.validationError = targetFileError;
	}

	public void setTotalNumberOfThreadsUsed(int totalNumberOfThreadsUsed) {
		this.totalNumberOfThreadsUsed = totalNumberOfThreadsUsed;
	}

	public void setTotalFilesAvailable(int totalFilesAvailable) {
		this.totalFilesAvailable = totalFilesAvailable;
	}

	public void incrementTotalNumberOfThreadsUsed() {
		this.totalNumberOfThreadsUsed++;
	}

	public void addStatisticalDataToList(TransformerStatisticalData stat) {
		this.statList.add(stat);
	}

	public void addAllFilesToFileToProcessList(List<File> files) {
		this.listOfFilesToProcess.addAll(files);
	}

	public void addFileToProccessList(File file) {
		this.listOfFilesToProcess.add(file);
	}

	public void setExemptedSheetList(List<String> exemptedSheetList) {
		this.exemptedSheetList.addAll(exemptedSheetList);
	}

	public void adddSheetToExemptedList(String exemptedSheet) {
		this.exemptedSheetList.add(exemptedSheet);
	}

	public void setSkipHiddenSheets(boolean skipHiddenSheets) {
		this.skipHiddenSheets = skipHiddenSheets;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
	
	public ErrorCatalog getValidationFileErrorCatalog() {
		return validationFileErrorCatalog;
	}

	public void setValidationFileErrorCatalog(ErrorCatalog targetFileErrorCatalog) {
		this.validationFileErrorCatalog = targetFileErrorCatalog;
	}

	
	public String generateReport() throws BusinessException {
		logger.info("generarting report");
		StringBuffer reportContent = new StringBuffer();
		addHeaderContent(reportContent);
		generateGeneralReport(reportContent);
		if(this.validationError) {
			generateOtherDetailsReport(reportContent);
		} else {
			generateDetailedReport(reportContent);
		}
		addFooterContent(reportContent);
		printReportInLog();
		String reportFilePath = saveReport(reportContent.toString());
		logger.info("Flushing Data from Report Manager");
		resetReportManager();
		return reportFilePath;
	}

	private void generateOtherDetailsReport(StringBuffer reportContent) {
		reportContent.append("<h2>Detailed Report</h2>");
		reportContent.append("<table>");
		addRowToReport(reportContent, "<b><font color='red'>Error Occured while processing. </font></b>", "");
		addRowToReport(reportContent, "<b><font color='red'>Error Code</font></b>", "<b><font color='red'>" + this.validationFileErrorCatalog.getErrorCode() + "</font></b>");
		addRowToReport(reportContent, "<b><font color='red'>Error Message</font></b>", "<b><font color='red'>" + this.validationFileErrorCatalog.getErrorMessage() + "</font></b>");
		reportContent.append("</table>");
	}

	private void printReportInLog() {
		StringBuffer reportContent = new StringBuffer();
		generateGeneralReportLog(reportContent);
		if(this.validationError) {
			generateOtherDetailsReportLog(reportContent);
		} else {
			generateDetailedReportLog(reportContent);
		}
		System.out.println(reportContent.toString());
	}

	private void generateOtherDetailsReportLog(StringBuffer reportContent) {
		reportContent.append("*************************** Detailed Report ***************************");
		reportContent.append("\n\n");
		reportContent.append("Error Occured while processing:\t\n");
		reportContent.append("\tError Code:\t" + this.validationFileErrorCatalog.getErrorCode() + "\n");
		reportContent.append("\tError Message:\t" + this.validationFileErrorCatalog.getErrorMessage() + "\n");
		reportContent.append("\n\n");
		
	}

	private void addFooterContent(StringBuffer reportContent) {
		reportContent.append("</body>");
		reportContent.append("</html>");
	}

	private void addHeaderContent(StringBuffer reportContent) {
		reportContent.append("<html>");
		reportContent.append("<head>");
		reportContent.append("<title>" + this.reportName + "</title>");
		reportContent.append("<style>");
		reportContent.append("table {");
		reportContent.append("  font-family: arial, sans-serif;");
		reportContent.append("  border-collapse: collapse;");
		reportContent.append("  width: 100%;");
		reportContent.append("}");
		reportContent.append("");
		reportContent.append("td, th {");
		reportContent.append("  border: 1px solid #dddddd;");
		reportContent.append("  text-align: left;");
		reportContent.append("  padding: 8px;");
		//reportContent.append("  width: 30%;");
		reportContent.append("}");
		reportContent.append("");
		reportContent.append("tbody tr:nth-child(odd) {");
		reportContent.append("  background-color: #dddddd;");
		reportContent.append("}");
		reportContent.append("</style>");
		reportContent.append("</head>");
		reportContent.append("<body>");
		reportContent.append("<h2>Transformation Report </h2>");
		
	}
	
	private void generateGeneralReport(StringBuffer reportContent) {
		reportContent.append("<h2>General Information</h2>");
		reportContent.append("<table>");
		addRowToReport(reportContent, "Report Name", this.reportName);
		addRowToReport(reportContent, "Report Date Time", getFormattedDateTime(new Date()));
		addRowToReport(reportContent, "Skipped Tabs", this.exemptedSheetList.stream().collect(Collectors.joining(",")));
		addRowToReport(reportContent, "Skip Hidden Tabs", this.skipHiddenSheets);
		addRowToReport(reportContent, "Bulk Processing", this.bulkProcessing);
		addRowToReport(reportContent, "Target Version", this.targetVersion);
		if (!this.isValidationError() && this.bulkProcessing) {
			addRowToReport(reportContent, "Total Number of Files Available", this.totalFilesAvailable);
			addRowToReport(reportContent, "List of Files To Process", getFileListAsString());
			addRowToReport(reportContent, "Total Success Files", this.totalSuccessFiles);
			addRowToReport(reportContent, "Total Failed Files", this.totalFailedFiles);
			addRowToReport(reportContent, "Input Directory", this.inputDirectory);
			addRowToReport(reportContent, "Output Directory", this.outputDirectory);
			addRowToReport(reportContent, "Number of Threads used", this.totalNumberOfThreadsUsed);
			addRowToReport(reportContent, "Start Time", getFormattedDateTime(this.startTime));
			addRowToReport(reportContent, "End Time", getFormattedDateTime(this.endTime));
			addRowToReport(reportContent, "Total Time to Process(ms)", (this.endTime.getTime() - this.startTime.getTime()));
		} else {
			addRowToReport(reportContent, "Processing Status", (this.validationError  || (!this.statList.isEmpty() && this.statList.get(0).hasError()) ? "<font color='red'><b>FAILED</b></font>" : "<font color='green'><b>SUCCESS</b></font>"));
		}
		reportContent.append("</table>");
		
	}
	
	private void addRowToReport(StringBuffer reportContent, String header, Object content) {
		reportContent.append("<tr>");
		reportContent.append("<td><b>" + header + "</b></td>");
		reportContent.append("<td>" + content + "</td>");
		reportContent.append("</tr>");

	}

	private void addRowToReport(StringBuffer reportContent, String header, Object content, int indentation) {
		reportContent.append("<tr>");
		if(indentation == 0) {
			reportContent.append("<td colspan='4'><b>" + header + "</b></td>");
			reportContent.append("<td colspan='16'>" + content + "</td>");
		} else if(indentation == 1) {
			reportContent.append("<td>" + "" + "</td>");
			reportContent.append("<td colspan='4'><b>" + header + "</b></td>");
			reportContent.append("<td colspan='15'>" + content + "</td>");
		} else if(indentation == 2) {
			reportContent.append("<td>" + "" + "</td>");
			reportContent.append("<td>" + "" + "</td>");
			reportContent.append("<td colspan='4'><b>" + header + "</b></td>");
			reportContent.append("<td colspan='14'>" + content + "</td>");
		}
		reportContent.append("</tr>");
		
	}
	
	private void generateDetailedReport(StringBuffer reportContent) {
		reportContent.append("<h2>Detailed Report</h2>");
		getTableStructure(reportContent);
		
		for (TransformerStatisticalData stat : this.statList) {
			int columnNotFoundInTargetCounter = 1;
			if(!stat.hasError())
				addRowToReport(reportContent, "<b><font color='green'>Input File Name</font></b>", "<b><font color='green'>" + stat.getInputFile() + "</font></b>", 0);
			else {
				addRowToReport(reportContent, "<b><font color='red'>Input File Name</font></b>", "<b><font color='red'>" + stat.getInputFile() + "</font></b>", 0);
			}
			if(!stat.hasError()){
				addRowToReport(reportContent, "Output Directory", stat.getOutputFileDir(), 1);
				addRowToReport(reportContent, "Output File Name", stat.getOutputFile(), 1);
				addRowToReport(reportContent, "Number of tabs in source file", stat.getNumOfTabsInSource(), 1);
				addRowToReport(reportContent, "Hidden Tabs", stat.getHiddenSheetList().stream().collect(Collectors.joining(",")), 1);
				addRowToReport(reportContent, "Tabs not found in Target", stat.getMissingSheetList().stream().collect(Collectors.joining(",")), 1);
				addRowToReport(reportContent, "Columns Not found in Target", "", 1);
				
				Map<String, List<String>> missingColumnList = stat.getAllMissingColumnList();
				Set<Entry<String, List<String>>> missingColumnEntrySet = missingColumnList.entrySet();
				for (Entry<String, List<String>> missingColumnEntry : missingColumnEntrySet) {
					addRowToReport(reportContent, columnNotFoundInTargetCounter + ". " + missingColumnEntry.getKey(), missingColumnEntry.getValue().stream().collect(Collectors.joining(",")), 2);
					columnNotFoundInTargetCounter++;
				}
				addRowToReport(reportContent, "Starting Time", getFormattedDateTime(stat.getStartTime()), 1);
				addRowToReport(reportContent, "Ending Time", getFormattedDateTime(stat.getEndTime()), 1);
				addRowToReport(reportContent, "Total time taken (ms)", (stat.getEndTime().getTime() - stat.getStartTime().getTime()), 1);
				
			} else {
				addRowToReport(reportContent, "<font color='red'>Errors encountered while parsing the file. Below are the details:</font>", "", 1);
				ErrorCatalog errCatalog = stat.getErrorCatalog();
				addRowToReport(reportContent, "<font color='red'>Error Code</font>", "<font color='red'>" + errCatalog.getErrorCode() + "</font>", 1);
				addRowToReport(reportContent, "<font color='red'>Error Message</font>", "<font color='red'>" + errCatalog.getErrorMessage() + "</font>", 1);
				
			}
		}
		
		reportContent.append("</table>");
		
	}

	private void getTableStructure(StringBuffer reportContent) {
		reportContent.append("<table width='100%' cellpadding='3px' cellspacing='2px' border='1'>");
		reportContent.append("<thead>");
		reportContent.append("<tr style='visibility: hidden;'>");
		reportContent.append("<th width='5%'></th>");
		reportContent.append("<th width='5%'></th>");
		reportContent.append("<th width='5%'></th>");
		reportContent.append("<th width='5%'></th>");
		reportContent.append("<th width='5%'></th>");
		reportContent.append("<th width='5%'></th>");
		reportContent.append("<th width='5%'></th>");
		reportContent.append("<th width='5%'></th>");
		reportContent.append("<th width='5%'></th>");
		reportContent.append("<th width='5%'></th>");
		reportContent.append("<th width='5%'></th>");
		reportContent.append("<th width='5%'></th>");
		reportContent.append("<th width='5%'></th>");
		reportContent.append("<th width='5%'></th>");
		reportContent.append("<th width='5%'></th>");
		reportContent.append("<th width='5%'></th>");
		reportContent.append("<th width='5%'></th>");
		reportContent.append("<th width='5%'></th>");
		reportContent.append("<th width='5%'></th>");
		reportContent.append("<th width='5%'></th>");
		reportContent.append("</tr>");
		reportContent.append("</thead>");
		reportContent.append("<tbody>");
	}

	private void generateDetailedReportLog(StringBuffer reportContent) {
		reportContent.append("*************************** Detailed Report ***************************");
		reportContent.append("\n\n");
		for (TransformerStatisticalData stat : this.statList) {
			int columnNotFoundInTargetCounter = 1;
			
			reportContent.append("Input File Name:\t" + stat.getInputFile() + "\n");
			reportContent.append("\n");
			if(!stat.hasError()){
				reportContent.append("\tOutput Directory:\t" + stat.getOutputFileDir() + "\n");
				reportContent.append("\tOutput File Name:\t" + stat.getOutputFile() + "\n");
				reportContent.append("\tNumber of tabs in source file:\t" + stat.getNumOfTabsInSource() + "\n");
				reportContent.append("\tHidden Tabs:\t" + stat.getHiddenSheetList().stream().collect(Collectors.joining(",")) + "\n");
				reportContent.append("\tTabs not found in Target:\t" + stat.getMissingSheetList().stream().collect(Collectors.joining(",")) + "\n");
				reportContent.append("\n");
				reportContent.append("\tColumns Not found in Target:\n");
				Map<String, List<String>> missingColumnList = stat.getAllMissingColumnList();
				Set<Entry<String, List<String>>> missingColumnEntrySet = missingColumnList.entrySet();
				for (Entry<String, List<String>> missingColumnEntry : missingColumnEntrySet) {
					reportContent.append("\t\t" + columnNotFoundInTargetCounter +". " + missingColumnEntry.getKey() + "\t:\t" + missingColumnEntry.getValue().stream().collect(Collectors.joining(",")) + "\n");
					columnNotFoundInTargetCounter++;
				}
				//reportContent.append("\t\t" + "" +". Tab Name\t:\tColumn Name\n");
				reportContent.append("\n");
				//reportContent.append("\t\t	Columns Skipped from Target\t");
				//reportContent.append("\t\t\t1. Tab Name : Column Name\t");
				//reportContent.append("\n");
				//reportContent.append("\t\t	Skipped Tabs from Target:\t");
				//reportContent.append("\n");
				reportContent.append("\tStarting Time:\t" + getFormattedDateTime(stat.getStartTime()) + "\n");
				reportContent.append("\tEnding Time:\t" + getFormattedDateTime(stat.getEndTime()) + "\n");
				reportContent.append("\tTotal time taken (ms):\t" + (stat.getEndTime().getTime() - stat.getStartTime().getTime()) + "\n");
			} else {
				reportContent.append("\tErrors encountered while parsing the file. Below are the details: \n");
				reportContent.append("\n");
				ErrorCatalog errCatalog = stat.getErrorCatalog();
				reportContent.append("\tError Code:\t" + errCatalog.getErrorCode() + "\n");
				reportContent.append("\tError Message:\t" + errCatalog.getErrorMessage() + "\n");
				reportContent.append("\n");
				
			}
			reportContent.append("\n\n");
		}
		
	}
	
	
	private void generateGeneralReportLog(StringBuffer reportContent) {
		reportContent.append("*************************** Transformation Report ***************************");
		reportContent.append("\n\n");
		reportContent.append("*************************** General Information ***************************");
		reportContent.append("\n\n");
		reportContent.append("Report Name:\t" + reportName + "\n");
		reportContent.append("Report Date Time:\t" + getFormattedDateTime(new Date()) + "\n");
		reportContent.append("Skipped Tabs:\t" + this.exemptedSheetList.stream().collect(Collectors.joining(",")) + "\n");
		reportContent.append("Skip Hidden Tabs:\t" + this.skipHiddenSheets + "\n");
		reportContent.append("Bulk Processing:\t" + this.bulkProcessing + "\n");
		reportContent.append("Target Version:\t" + this.targetVersion + "\n");
		if (!this.isValidationError() && this.bulkProcessing) {
			reportContent.append("Total Number of Files Available:\t" + this.totalFilesAvailable + "\n");
			reportContent.append("List of Files To Process:\t" + getFileListAsString() + "\n");
			reportContent.append("Total Success Files:\t" + this.totalSuccessFiles + "\n");
			reportContent.append("Total Failed Files:\t" + this.totalFailedFiles + "\n");
			reportContent.append("Input Directory:\t" + this.inputDirectory + "\n");
			reportContent.append("Output Directory:\t" + this.outputDirectory + "\n");
			reportContent.append("Number of Threads used:\t" + this.totalNumberOfThreadsUsed + "\n");
			reportContent.append("Start Time:\t" + getFormattedDateTime(this.startTime) + "\n");
			reportContent.append("End Time:\t" + getFormattedDateTime(this.endTime) + "\n");
			reportContent.append("Total Time to Process(ms):\t" + (this.endTime.getTime() - this.startTime.getTime()) + "\n");
		} else {
			reportContent.append("Processing Status:\t" + (this.validationError  || (!this.statList.isEmpty() && this.statList.get(0).hasError()) ? "FAILED" : "SUCCESS") + "\n");
		}
		reportContent.append("\n\n");
		
	}

	private String saveReport(String reportContent) throws BusinessException {
		logger.info("Saving report file");
		String reportFilePath = null;
		try {
			reportFilePath = ConversionUtil.getTargetTemplateFileName(this.reportName, "output-reports");
			Path path = Paths.get(reportFilePath);
			Files.write(path, reportContent.getBytes());

		} catch (URISyntaxException | IOException e) {
			throw new BusinessException("Unknown System Exception while saving report: " + e.getMessage());
		}
		return reportFilePath;
	}

	public void generateAndSetReportName(String name) {
		String reportName = null;
		try {
			File inputFile = new File(name);
			if (inputFile.isDirectory()) {
				String[] folderArr = name.split(Pattern.quote(File.separator));
				String dir = folderArr[folderArr.length - 1];
				long timestamp = (new Date()).getTime();
				reportName = dir + "_" + timestamp + ".html";
			} else {
				String[] folderArr = name.split(Pattern.quote(File.separator));
				String fileName = folderArr[folderArr.length - 1].replace(".xlsx", "");
				long timestamp = (new Date()).getTime();
				reportName = fileName + "_" + timestamp + ".html";
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		this.reportName = reportName;
	}

	private String getFileListAsString() {
		StringBuffer fileNameSb = new StringBuffer();
		String returnValue = "";
		if(!listOfFilesToProcess.isEmpty()) {
			for (File file : listOfFilesToProcess) {
				fileNameSb.append(file.getName());
				fileNameSb.append(",");
			}
		
			returnValue = fileNameSb.toString().substring(0, fileNameSb.lastIndexOf(","));
		} 
		return returnValue;
		
	}

	private String getFormattedDateTime(Date date) {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return formatter.format(date);
	}


}
