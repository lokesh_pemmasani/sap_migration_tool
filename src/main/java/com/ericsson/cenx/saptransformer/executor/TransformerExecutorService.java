package src.main.java.com.ericsson.cenx.saptransformer.executor;

import java.io.File;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;

import src.main.java.com.ericsson.cenx.saptransformer.TransformationDetails;
import src.main.java.com.ericsson.cenx.saptransformer.exception.BusinessException;
import src.main.java.com.ericsson.cenx.saptransformer.stats.ReportManager;
import src.main.java.com.ericsson.cenx.saptransformer.util.ConversionUtil;
import src.main.java.com.ericsson.cenx.saptransformer.util.TransformerFileFilter;

public class TransformerExecutorService {
	
	static Logger logger = Logger.getLogger(TransformerExecutorService.class.getName());
	
	public static void execute(TransformationDetails transformationDetailsTO) throws BusinessException{	
		File sourceFileDir = transformationDetailsTO.getSourceDirectory();
		List<File> inputFileList = TransformerFileFilter.findFiles(sourceFileDir);
		int numberOfFiles = inputFileList.size();
		
		int numberOfThreads = ConversionUtil.determineNumberOfThreadsToUse(sourceFileDir, numberOfFiles);
		
		ReportManager reportManager = ReportManager.getInstance();
		reportManager.setTotalNumberOfThreadsUsed(numberOfThreads);
		reportManager.setTotalFilesAvailable(numberOfFiles);
		reportManager.addAllFilesToFileToProcessList(inputFileList);
		
		logger.info("Number of threads to be used [" + numberOfThreads + "]");
		
		logger.info("Creating executor for operation");
		ExecutorService transformerExecutor = Executors.newFixedThreadPool(numberOfThreads);
		
		for (File inputFile : inputFileList) {
			logger.info("Assigning file[" + inputFile.getName() + "] to transform task");
			transformationDetailsTO.setSourceFile(inputFile);
			TransformerTask transformerThread = new TransformerTask(transformationDetailsTO);
			logger.info("Submitting the job to queue");
			transformerExecutor.submit(transformerThread);
		}
		
		transformerExecutor.shutdown();

		try {
			transformerExecutor.awaitTermination(60, TimeUnit.MINUTES);
			logger.info("Executor engine stopped");
		} catch (InterruptedException e) {
			e.printStackTrace();
			transformerExecutor.shutdownNow();
		}
	}
}
