package src.main.java.com.ericsson.cenx.saptransformer.executor;

import java.io.File;
import java.util.List;

import org.apache.log4j.Logger;

import src.main.java.com.ericsson.cenx.saptransformer.ExcelTransformer;
import src.main.java.com.ericsson.cenx.saptransformer.TransformationDetails;
import src.main.java.com.ericsson.cenx.saptransformer.exception.BusinessException;

public class TransformerTask implements Runnable {
	
	static Logger logger = Logger.getLogger(TransformerExecutorService.class.getName());
	
	private File sourceFile;
	private File targetFile; 
	private File outputDir; 
	private List<String> exemptedSheetList;
	private boolean skipHiddenSheets;
	private boolean appendVersionInOutput;
	
	
	public TransformerTask(TransformationDetails transformationDetailsTO) {
		this.sourceFile = transformationDetailsTO.getSourceFile(); 
		this.targetFile = transformationDetailsTO.getTargetFile();
		this.outputDir = transformationDetailsTO.getOutputDir();
		this.exemptedSheetList = transformationDetailsTO.getExemptedSheetList();
		this.skipHiddenSheets = transformationDetailsTO.isSkipHiddenSheets();
		this.appendVersionInOutput = transformationDetailsTO.isAppendVersionInOutput();
	}

	@Override
	public void run() {
		if (logger.isDebugEnabled()) {
			logger.debug("File [" + this.sourceFile + "] is processed by thread [" + Thread.currentThread().getId() + ":" + Thread.currentThread().getName() + "]");
		}
		TransformationDetails transformationDetailsTO = new TransformationDetails();
		
		transformationDetailsTO.setSourceFile(sourceFile);
		transformationDetailsTO.setTargetFile(targetFile);
		transformationDetailsTO.setOutputDir(outputDir);
		transformationDetailsTO.setExemptedSheetList(exemptedSheetList);
		transformationDetailsTO.setSkipHiddenSheets(skipHiddenSheets);
		transformationDetailsTO.setAppendVersionInOutput(appendVersionInOutput);
		
		try {
			ExcelTransformer.performCompare(transformationDetailsTO);
		} catch (BusinessException e) {
			logger.error("Error parsing file: " + e.getMessage());
		}
	}

}
