package src.main.java.com.ericsson.cenx.saptransformer.tools.gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GraphicsEnvironment;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.PrintStream;
import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;
import java.util.Date;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.filechooser.FileFilter;
import javax.swing.text.BadLocationException;


import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import org.apache.commons.io.FilenameUtils;

import src.main.java.com.ericsson.cenx.saptransformer.ExcelTransformer;
import src.main.java.com.ericsson.cenx.saptransformer.TransformationDetails;
import src.main.java.com.ericsson.cenx.saptransformer.exception.BusinessException;
import src.main.java.com.ericsson.cenx.saptransformer.executor.TransformerExecutorService;
import src.main.java.com.ericsson.cenx.saptransformer.stats.LogCustomizer;
import src.main.java.com.ericsson.cenx.saptransformer.stats.ReportManager;
import src.main.java.com.ericsson.cenx.saptransformer.util.ConversionUtil;
import src.main.java.com.ericsson.cenx.saptransformer.util.CustomOutputStream;
import src.main.java.com.ericsson.cenx.saptransformer.util.PropertiesCache;
import src.main.java.com.ericsson.cenx.saptransformer.validators.ValidationManager;

public class TransformExcelFilesGui extends JFrame {

	private static final long serialVersionUID = -4211222673528941125L;

	private int WINDOW_DEFAULT_WIDTH = 1600;
	private int WINDOW_DEFAULT_HEIGHT = 800;

	private JTextArea textArea;

	private JButton transformButton = new JButton("Transform");
	private JButton buttonClear = new JButton("Clear");
	private JButton uploadTargetTemplateButton = new JButton("Add Template");
	private JButton addVersionButton = new JButton("Add To System");
	private JButton srcFileButton = new JButton("Source File ...");
	private JButton outputDirButton = new JButton("Output Folder ...");
	private JButton deleteTemplateButton = new JButton("Delete Template");

	private JCheckBox bulkOperationCB = new JCheckBox("Bulk Proccessing");
	private JCheckBox enableDebugCB = new JCheckBox("Enable Debug");
	private JCheckBox skipHiddenSheetsCB = new JCheckBox("Skip Hidden Sheets");
	private JCheckBox versionAppendCB = new JCheckBox("Append Version in output file");
	private JCheckBox enableLoggingCB = new JCheckBox("Enable Logging");

	private JTextField sheetNameFilterTF = new JTextField(50);
	private JTextField outputDirTF = new JTextField();
	private JTextField srcFileTF = new JTextField();
	private JTextField targetFileTF = new JTextField();
	private JTextField targetVersionTF = new JTextField(10);
	private JTextField templateSrcFileTF = new JTextField();
	
	private JTextField reportLocationTF = new JTextField(10);

	private JComboBox<String> targetVersionsComboBox = new JComboBox<String>();
	private JComboBox<String> versionsComboBox = new JComboBox<String>();

	final static Color ERROR_COLOR = Color.RED;

	//private PrintStream standardOut;

	private static Logger logger;

	public TransformExcelFilesGui(String[] args) {
		super("Excel Transform Tool");

		determineWindowSize();

		initLoggingScreen();
		// creates the GUI
		getContentPane().setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));

		JPanel buttonsPanel = new JPanel();
		buttonsPanel.setLayout(new BoxLayout(buttonsPanel, BoxLayout.Y_AXIS));

		addSrcFileButton(buttonsPanel, args);
		addCompareFileButton(buttonsPanel, args);
		addDirButton(buttonsPanel, args);
		buttonsPanel.add(Box.createVerticalStrut(5));
		addSheetNameFilter(args, buttonsPanel);

		buttonsPanel.add(Box.createVerticalStrut(5));

		Box controlBox = Box.createVerticalBox();
		controlBox.add(buttonsPanel);
		controlBox.add(Box.createVerticalStrut(5));

		Box extendedFeatureBox = Box.createHorizontalBox();

		Box advancedOptionsBox = Box.createHorizontalBox();
		advancedOptionsBox.setBorder(BorderFactory.createTitledBorder("Advanced Options"));

		Box advancedOptionBoxLP = Box.createVerticalBox();
		addAdvancedOptionsBoxLP(advancedOptionsBox, advancedOptionBoxLP);

		Box adminOptionsBox = Box.createHorizontalBox();
		adminOptionsBox.setBorder(BorderFactory.createTitledBorder("Admin Options"));

		Box addTemplateBox = Box.createVerticalBox();
		preapreAddTemplateBox(addTemplateBox);

		Box deleteTemplateBox = Box.createVerticalBox();
		prepareDeleteTemplateBox(deleteTemplateBox);

		JTabbedPane configTab = new JTabbedPane();
		configTab.addTab("Add Template", null, addTemplateBox);
		configTab.addTab("Delete Template", null, deleteTemplateBox);

		advancedOptionsBox.add(Box.createVerticalGlue());
		extendedFeatureBox.add(advancedOptionsBox);

		adminOptionsBox.add(configTab);
		adminOptionsBox.add(Box.createVerticalGlue());
		extendedFeatureBox.add(adminOptionsBox);

		controlBox.add(extendedFeatureBox);
		controlBox.add(Box.createVerticalStrut(5));

		addTransformButton(controlBox);

		controlBox.setPreferredSize(new Dimension(WINDOW_DEFAULT_WIDTH, 200));
		javax.swing.JScrollPane logScroll = new JScrollPane(textArea);

		JSplitPane splitPaneCenter = new JSplitPane(JSplitPane.VERTICAL_SPLIT, controlBox, logScroll);
		splitPaneCenter.setOneTouchExpandable(true);
		splitPaneCenter.setResizeWeight(0.1);

		add(splitPaneCenter);

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(WINDOW_DEFAULT_WIDTH, WINDOW_DEFAULT_HEIGHT);
		setLocationRelativeTo(null); // centers on screen

		showSystemOptions();
		if (args != null && args.length > 0) {
			performCompare(args);
		}
	}

	private void determineWindowSize() {
		Rectangle screenBounds = GraphicsEnvironment.getLocalGraphicsEnvironment().getMaximumWindowBounds();
		if (WINDOW_DEFAULT_WIDTH > screenBounds.getWidth()) {
			WINDOW_DEFAULT_WIDTH = (int) screenBounds.getWidth();
		}
		if (WINDOW_DEFAULT_HEIGHT > screenBounds.getHeight()) {
			WINDOW_DEFAULT_HEIGHT = (int) screenBounds.getHeight();
		}
	}

	private void addTransformButton(Box controlBox) {

		transformButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent evt) {
				if (validateTransformInput()) {
					reportLocationTF.setText("");
					performCompare(null);
				}
			}
		});

		buttonClear.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent evt) {
				try {
					textArea.getDocument().remove(0, textArea.getDocument().getLength());
					//standardOut.println("Text area cleared");
				} catch (BadLocationException ex) {
					ex.printStackTrace();
				}
			}
		});

		/*
		 * verifyButton.addActionListener(new ActionListener() {
		 * 
		 * @Override public void actionPerformed(ActionEvent evt) { // activated the
		 * transform Button logger.
		 * info("TBD: Will verify the contents of the input and then activate the transform button"
		 * ); // transformButton.setEnabled(!StringUtils.isEmpty(srcFileTF.getText()) &&
		 * // !StringUtils.isEmpty(targetFileTF.getText()) && //
		 * !StringUtils.isEmpty(outputDirTF.getText())); } });
		 */

		Box hbox = Box.createHorizontalBox();
		hbox.add(transformButton);
		hbox.add(buttonClear);
		// hbox.add(verifyButton);
		controlBox.add(hbox);

	}

	private void addSheetNameFilter(String[] args, JPanel buttonsPanel) {
		JLabel sheetNameL = new JLabel("  Exempted Tabs (\",\" seperated list, optional):");
		sheetNameFilterTF.setMaximumSize(new Dimension(150, 25));
		if (args.length > 2) {
			sheetNameFilterTF.setText(args[2]);
		}
		sheetNameFilterTF
				.setText("README,Template Info,Tracking History,Constraints,Data Source (Kafka),Data Source (SNMP)");
		addLabelWithTextField(buttonsPanel, sheetNameL, sheetNameFilterTF);
	}

	private void initLoggingScreen() {
		textArea = new JTextArea(20, 15);
		textArea.setEditable(false);
		PrintStream printStream = new PrintStream(new CustomOutputStream(textArea));

		// keeps reference of standard output stream
		//standardOut = System.out;

		// re-assigns standard output stream and error output stream
		System.setOut(printStream);
		System.setErr(printStream);
	}

	private void addAdvancedOptionsBoxLP(Box parent, Box advancedOptionBox) {

		bulkOperationCB.setAlignmentX(Box.LEFT_ALIGNMENT);
		bulkOperationCB.setSelected(false);
		advancedOptionBox.add(bulkOperationCB);
		advancedOptionBox.add(Box.createHorizontalStrut(10));

		versionAppendCB.setAlignmentX(Box.LEFT_ALIGNMENT);
		versionAppendCB.setSelected(false);
		advancedOptionBox.add(versionAppendCB);
		advancedOptionBox.add(Box.createHorizontalStrut(10));

		skipHiddenSheetsCB.setAlignmentX(Box.LEFT_ALIGNMENT);
		skipHiddenSheetsCB.setSelected(true);
		advancedOptionBox.add(skipHiddenSheetsCB);
		advancedOptionBox.add(Box.createHorizontalStrut(10));

		Box horiz = Box.createHorizontalBox();
		horiz.setAlignmentX(Box.LEFT_ALIGNMENT);

		enableLoggingCB.setSelected(true);
		horiz.add(enableLoggingCB);
		horiz.add(Box.createHorizontalStrut(10));
		// enableDebugCB.setEnabled(true);
		enableDebugCB.setSelected(false);
		horiz.add(enableDebugCB);
		horiz.add(Box.createHorizontalStrut(10));

		advancedOptionBox.add(horiz);
		JLabel reportLocationLabel = new JLabel("Report Avaliable at: ");
		
		horiz = Box.createHorizontalBox();
		horiz.setAlignmentX(Box.LEFT_ALIGNMENT);
		horiz.add(reportLocationLabel);
		horiz.add(Box.createHorizontalStrut(10));
		reportLocationTF.setEditable(false);
		//reportLocationTF.setText("C:\\Others\\Office\\eclipse_ws_vcp_branches\\sap-transformer\\target\\classes\\sap-transformer\\output-reports\\output.txt");
		reportLocationTF.setMaximumSize(new Dimension(20000,25));
		horiz.add(reportLocationTF);
		advancedOptionBox.add(horiz);
		
		versionAppendCB.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent evt) {

				if (versionAppendCB.isSelected()) {
					logger.info("Target Version will be appended in the outfile(s)");
				} else {
					logger.info("Debug disabled");
				}
			}
		});

		enableDebugCB.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent evt) {

				if (enableDebugCB.isSelected()) {
					logger.info("Debug enabled");
					LogCustomizer.changeLogLevel(true);
				} else {
					logger.info("Debug disabled");
					LogCustomizer.changeLogLevel(false);
				}
			}
		});

		bulkOperationCB.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent evt) {

				if (bulkOperationCB.isSelected()) {
					logger.info("Bulk processing enabled");
					srcFileTF.setText("");
					srcFileButton.setText("Source Directory");
				} else {
					logger.info("Bulk processing disabled");
					srcFileButton.setText("Source File ...");
				}
			}
		});

		skipHiddenSheetsCB.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent evt) {

				if (skipHiddenSheetsCB.isSelected()) {
					logger.info("Hidden Sheets in the source file(s) will be skipped");
				} else {
					logger.info("Skip Hidden Sheets option is disabled");
				}
			}
		});

		enableLoggingCB.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent evt) {

				if (enableLoggingCB.isSelected()) {
					logger.info("Enabling Logging");
					enableDebugCB.setEnabled(true);
					LogCustomizer.switchLogging(false);
				} else {
					logger.info("Disabling Logging");
					enableDebugCB.setEnabled(false);
					LogCustomizer.switchLogging(true);
				}
			}
		});
		advancedOptionBox.add(Box.createVerticalGlue());
		parent.add(advancedOptionBox);

	}

	private void preapreAddTemplateBox(Box advancedOptionBox) {
		Box horiz = Box.createHorizontalBox();
		horiz.setAlignmentX(Box.LEFT_ALIGNMENT);
		horiz.add(uploadTargetTemplateButton);
		horiz.add(Box.createHorizontalStrut(10));
		horiz.add(Box.createHorizontalStrut(10));

		JLabel targetTemplateVersionLabel = new JLabel("Target Version");
		horiz.add(Box.createHorizontalStrut(10));

		targetVersionTF.setMaximumSize(new Dimension(150, 25));
		addLabelWithTextField(horiz, targetTemplateVersionLabel, targetVersionTF);

		advancedOptionBox.add(horiz);
		advancedOptionBox.add(Box.createHorizontalStrut(10));
		templateSrcFileTF.setMaximumSize(new Dimension(500, 25));
		templateSrcFileTF.setAlignmentX(Box.LEFT_ALIGNMENT);
		advancedOptionBox.add(templateSrcFileTF);
		advancedOptionBox.add(Box.createHorizontalStrut(10));
		advancedOptionBox.add(addVersionButton);

		advancedOptionBox.add(Box.createVerticalGlue());

		uploadTargetTemplateButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				JFileChooser chooser = new JFileChooser();
				
				chooser.setFileFilter(new FileFilter() {

					public String getDescription() {
						return "Excel Documents (.xlsx)";
					}

					public boolean accept(File f) {
						if (f.isDirectory()) {
							return true;
						} else {
							String filename = f.getName().toLowerCase();
							return filename.endsWith(".xlsx");
						}
					}
				});
				
				if (templateSrcFileTF.getText() != null) {
					File currentPath = new File(FilenameUtils.getFullPath(templateSrcFileTF.getText()));
					if (currentPath.exists()) {
						chooser.setCurrentDirectory(currentPath);
					}
				}

				int option = chooser.showOpenDialog(getContentPane());
				if (option == JFileChooser.APPROVE_OPTION) {
					templateSrcFileTF.setText(chooser.getSelectedFile().getAbsolutePath());
				}
			}
		});

		addVersionButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				if (validateAddTargetTemplateInput()) {
					String templateSrcFileString = templateSrcFileTF.getText();
					String targetVersion = targetVersionTF.getText();
					if (StringUtils.isNoneEmpty(templateSrcFileString) && StringUtils.isNoneEmpty(targetVersion)) {
						try {
							ValidationManager.validateUploadTemplate(templateSrcFileString, targetVersion);
							ConversionUtil.uploadTemplate(templateSrcFileString, targetVersion);

							ConversionUtil.loadPropertiesToComboBox(targetVersionsComboBox);
							ConversionUtil.loadPropertiesToComboBox(versionsComboBox);
							templateSrcFileTF.setText("");
							targetVersionTF.setText("");

						} catch (BusinessException e) {
							logger.error("Error Occured while uploading Template File");
							logger.error(e.getMessage());
							templateSrcFileTF.setText("");
							targetVersionTF.setText("");
						}
					}
				}
			}

		});
	}

	private void addDirButton(JPanel buttonsPanel, String[] args) {
		addButtonWithLabel(buttonsPanel, outputDirButton, outputDirTF);

		if (args.length > 0) {
			File tgtDir = ConversionUtil.getDefaultOutputFolderPath(new File(args[0]));
			outputDirTF.setText(tgtDir.getAbsolutePath());
		}

		outputDirButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				JFileChooser chooser = new JFileChooser();
				String startTxt = null;
				if (outputDirTF.getText() != null && outputDirTF.getText().length() > 0) {
					startTxt = outputDirTF.getText();
				} else {
					startTxt = srcFileTF.getText();
				}
				if (startTxt != null && startTxt.length() > 0) {
					File currentPath = new File(FilenameUtils.getFullPath(startTxt));
					if (currentPath.exists()) {
						chooser.setCurrentDirectory(currentPath);
					}
				}

				chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
				int option = chooser.showOpenDialog(getContentPane());
				if (option == JFileChooser.APPROVE_OPTION) {
					outputDirTF.setText(chooser.getSelectedFile().getAbsolutePath());
				}
			}
		});
	}

	private void addCompareFileButton(JPanel buttonsPanel, String[] args) {
		ConversionUtil.loadPropertiesToComboBox(targetVersionsComboBox);
		addComboBoxWithTextField(buttonsPanel, targetVersionsComboBox, targetFileTF);
		if (args.length > 1) {
			targetFileTF.setText(args[1]);
		}
		targetFileTF.setEditable(false);
		targetVersionsComboBox.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Object targetVersion = targetVersionsComboBox.getSelectedItem();
				if (targetVersion != null) {
					String targetTemplateFile = PropertiesCache.getInstance().getProperty((String) targetVersion);
					targetFileTF.setText(targetTemplateFile);
				}
			}

		});
	}

	private void addSrcFileButton(JPanel buttonsPanel, String[] args) {
		addButtonWithLabel(buttonsPanel, srcFileButton, srcFileTF);
		if (args.length > 0) {
			srcFileTF.setText(args[0]);
		}

		srcFileButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				JFileChooser fileChooser = new JFileChooser();

				fileChooser.setFileFilter(new FileFilter() {

					public String getDescription() {
						return "Excel Documents (.xlsx)";
					}

					public boolean accept(File f) {
						if (f.isDirectory()) {
							return true;
						} else {
							String filename = f.getName().toLowerCase();
							return filename.endsWith(".xlsx");
						}
					}
				});

				if (srcFileTF.getText() != null) {
					File currentPath = new File(FilenameUtils.getFullPath(srcFileTF.getText()));
					if (currentPath.exists()) {
						fileChooser.setCurrentDirectory(currentPath);
					}
				}
				if (bulkOperationCB.isSelected()) {
					fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
				}
				int option = fileChooser.showOpenDialog(getContentPane());
				if (option == JFileChooser.APPROVE_OPTION) {
					srcFileTF.setText(fileChooser.getSelectedFile().getAbsolutePath());
				}
			}
		});
	}

	private void addButtonWithLabel(JPanel parent, JButton button, JTextField textfield) {
		JPanel button_label = new JPanel();
		BoxLayout layout = new BoxLayout(button_label, BoxLayout.X_AXIS);
		button_label.setLayout(layout);
		button_label.add(button);
		button_label.add(textfield);
		button_label.add(Box.createHorizontalGlue());
		parent.add(button_label);
	}

	private void addComboBoxWithTextField(JPanel parent, JComboBox<String> comboBox, JTextField textfield) {
		JPanel comboBoxPanel = new JPanel();
		BoxLayout layout = new BoxLayout(comboBoxPanel, BoxLayout.X_AXIS);
		comboBoxPanel.setLayout(layout);
		JLabel versionLabel = new JLabel("Select Target Version");
		comboBoxPanel.add(versionLabel);
		comboBoxPanel.add(comboBox);
		comboBoxPanel.add(textfield);
		comboBoxPanel.add(Box.createHorizontalGlue());
		parent.add(comboBoxPanel);
	}

	private void addLabelWithTextField(JComponent parent, JLabel label, JTextField textfield) {
		Box button_label = Box.createHorizontalBox();
		button_label.add(label);
		button_label.add(textfield);
		button_label.add(Box.createHorizontalGlue());
		parent.add(button_label);
	}

	private void performCompare(String[] args) {

		SwingWorker excelTransformerWorker = new SwingWorker() {

			@Override
			protected Object doInBackground() throws Exception {
				if (args != null) {
					logger.info("Invoking the app from command promt is disabled");
					/*try {
						ExcelTransformer.main(args);
					} catch (EncryptedDocumentException | InvalidFormatException | IOException e) {
						e.printStackTrace();
					}*/
				} else {
					System.out.println("Processing Started");
					String targetVersion = null;
					if (targetVersionsComboBox.getSelectedItem() != null) {
						targetVersion = (String) targetVersionsComboBox.getSelectedItem();
					}

					String exemptedSheets = sheetNameFilterTF.getText();
					List<String> exemptedSheetList = ConversionUtil.getExemptedSheetList(exemptedSheets);

					boolean bulkOperation = false;
					if (bulkOperationCB.isSelected()) {
						bulkOperation = true;
					}

					TransformationDetails transformationDetailsTO = new TransformationDetails();
					transformationDetailsTO.setTargetFile(new File(targetFileTF.getText()));
					transformationDetailsTO.setOutputDir(new File(outputDirTF.getText()));
					transformationDetailsTO.setExemptedSheetList(exemptedSheetList);
					transformationDetailsTO.setSkipHiddenSheets(skipHiddenSheetsCB.isSelected());
					transformationDetailsTO.setAppendVersionInOutput(versionAppendCB.isSelected());
					transformationDetailsTO.setTargetVersion(targetVersion);
					transformationDetailsTO.setBulkOperation(bulkOperation);
					if(bulkOperation) {
						transformationDetailsTO.setSourceDirectory(new File(srcFileTF.getText()));
					}else {
						transformationDetailsTO.setSourceFile(new File(srcFileTF.getText()));
					}
					
					ReportManager reportManager = ReportManager.getInstance();
					reportManager.generateAndSetReportName(srcFileTF.getText());
					reportManager.setExemptedSheetList(transformationDetailsTO.getExemptedSheetList());
					reportManager.setSkipHiddenSheets(transformationDetailsTO.isSkipHiddenSheets());
					reportManager.setBulkProcessing(bulkOperation);
					reportManager.setTargetVersion(targetVersion);
					
					try {
						ValidationManager.validateTransformer(transformationDetailsTO);
						
						if (bulkOperation) {
							reportManager.setInputDirectory(srcFileTF.getText());
							reportManager.setOutputDirectory(outputDirTF.getText());
							reportManager.setStartTime(new Date());
							TransformerExecutorService.execute(transformationDetailsTO);
							reportManager.setEndTime(new Date());

						} else {
							
							ExcelTransformer.performCompare(transformationDetailsTO);
						}
						
						
					} catch (BusinessException e) {
						logger.error("Error in execution: " + e.getMessage());
					} finally {
						System.out.println("Proccessing Completed");
						String reportFilePath = "";
						try {
							reportFilePath = reportManager.generateReport();
						}catch(Exception e) {
							e.printStackTrace();
						}
						reportLocationTF.setText(reportFilePath);
					}
				}
				return null;
			}

			@Override
			protected void done() {
				transformButton.setEnabled(true);
				super.done();
			}

		};
		transformButton.setEnabled(false);
		excelTransformerWorker.execute();
	}

	private boolean validateTransformInput() {

		if (StringUtils.isBlank(srcFileTF.getText())) {
			JOptionPane.showMessageDialog(this, "Source File Cannot be Blank", "Validation Error",
					JOptionPane.ERROR_MESSAGE);
			return false;
		}
		if (StringUtils.isBlank(targetFileTF.getText())) {
			JOptionPane.showMessageDialog(this, "Target Version Cannot be Blank", "Validation Error",
					JOptionPane.ERROR_MESSAGE);
			return false;
		}
		if (StringUtils.isBlank(outputDirTF.getText())) {
			JOptionPane.showMessageDialog(this, "Output Directory Cannot be Blank", "Validation Error",
					JOptionPane.ERROR_MESSAGE);
			return false;
		}

		return true;

	}

	private boolean validateAddTargetTemplateInput() {

		if (StringUtils.isBlank(targetVersionTF.getText())) {
			JOptionPane.showMessageDialog(this, "Template File Cannot be Blank", "Validation Error",
					JOptionPane.ERROR_MESSAGE);
			return false;
		}
		if (StringUtils.isBlank(templateSrcFileTF.getText())) {
			JOptionPane.showMessageDialog(this, "Target Version Cannot be Blank", "Validation Error",
					JOptionPane.ERROR_MESSAGE);
			return false;
		}

		return true;
	}

	public static void main(String[] args) throws ClassNotFoundException, InstantiationException,
			IllegalAccessException, UnsupportedLookAndFeelException {
		UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				new TransformExcelFilesGui(args).setVisible(true);
			}
		});
	}

	private static void showSystemOptions() {
		logger = Logger.getLogger(TransformExcelFilesGui.class.getName());
		RuntimeMXBean runtimeMxBean = ManagementFactory.getRuntimeMXBean();
		List<String> vmarguments = runtimeMxBean.getInputArguments();
		logger.info("VMarguments: " + vmarguments);
		logger.info("Max memory: " + (Runtime.getRuntime().maxMemory() / (1024 * 1024)) + "MB");

	}

	private void prepareDeleteTemplateBox(Box advancedOptionBox) {

		Box horiz = Box.createHorizontalBox();
		horiz.setAlignmentX(Box.LEFT_ALIGNMENT);
		horiz.add(Box.createHorizontalStrut(10));
		JLabel versionLabel = new JLabel("Available Version");
		horiz.add(versionLabel);
		horiz.add(Box.createHorizontalStrut(10));
		versionsComboBox.setMaximumSize(new Dimension(100, 25));
		horiz.add(versionsComboBox);
		horiz.add(Box.createHorizontalStrut(10));
		horiz.add(deleteTemplateButton);
		horiz.add(Box.createHorizontalStrut(10));
		advancedOptionBox.add(horiz);

		ConversionUtil.loadPropertiesToComboBox(versionsComboBox);

		deleteTemplateButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				if(versionsComboBox.getItemCount() > 0){
					try {
							
						String version = (String) versionsComboBox.getSelectedItem();
						ValidationManager.validateDeleteTemplate(version);
						if (ConversionUtil.deleteTemplate(version)) {
							logger.info("Version successfully deleted from system");
							ConversionUtil.reloadComboBox(targetVersionsComboBox);
							ConversionUtil.reloadComboBox(versionsComboBox);
						} else {
							logger.info("Version cannot be deleted.");
						}
					}catch(BusinessException e) {
						logger.error("Error Occured while Deleting Template Version");
						logger.error(e.getMessage());
					}
				} else {
					logger.info("Nothing to Delete");
				}
			}
		});

	}

}