package src.main.java.com.ericsson.cenx.saptransformer;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import src.main.java.com.ericsson.cenx.saptransformer.exception.BusinessException;
import src.main.java.com.ericsson.cenx.saptransformer.stats.ErrorCatalog;
import src.main.java.com.ericsson.cenx.saptransformer.stats.ReportManager;
import src.main.java.com.ericsson.cenx.saptransformer.stats.TransformerStatisticalData;
import src.main.java.com.ericsson.cenx.saptransformer.util.ConversionUtil;
import src.main.java.com.ericsson.cenx.saptransformer.util.TransformerConstants;

public class ExcelTransformer {

	static Logger logger = Logger.getLogger(ExcelTransformer.class.getName());

	public static void performCompare(TransformationDetails transformationDetailsTO) throws BusinessException/* throws IOException, InvalidFormatException, FileNotFoundException */{	
		
		ConversionUtil.memoryInfo();
		
		ReportManager reportManager = ReportManager.getInstance();
		TransformerStatisticalData stat = new TransformerStatisticalData();
		reportManager.addStatisticalDataToList(stat);
		stat.setInputFile(transformationDetailsTO.getSourceFile().getAbsolutePath());
		stat.setOutputFileDir(transformationDetailsTO.getOutputDir().getAbsolutePath());
		
		File sourceFile = transformationDetailsTO.getSourceFile();
		File targetFile = transformationDetailsTO.getTargetFile();
		File dir = transformationDetailsTO.getOutputDir();
		List<String> exemptedSheetList = transformationDetailsTO.getExemptedSheetList();
		boolean skipHiddenSheets = transformationDetailsTO.isSkipHiddenSheets();
		stat.setStartTime(new Date());
		
		Workbook sourceWorkbook = null;
		Workbook targetWorkBook = null;
				
		try {
			logger.info("Loading  source file: " + sourceFile + "...");
			FileInputStream sourceFileIS = new FileInputStream(sourceFile);
			sourceWorkbook = WorkbookFactory.create(sourceFileIS);
			if ( sourceFileIS != null ) {
				sourceFileIS.close();
			}
			logger.info("Loading  source file complete");
		} catch(Throwable sourceFileThrowable) {
			if(sourceFileThrowable instanceof FileNotFoundException) {
				ErrorCatalog errorCatalog = new ErrorCatalog();
				errorCatalog.setFileName(transformationDetailsTO.getSourceFile().getName());
				errorCatalog.setErrorCode(TransformerConstants.CONST_ERR_CODE_FILENOTFOUND);
				errorCatalog.setErrorMessage(MessageFormat.format(TransformerConstants.CONST_ERR_MESSAGE_FILENOTFOUND, transformationDetailsTO.getSourceFile()));
				stat.setHasError(true);
				stat.setErrorCatalog(errorCatalog);
				reportManager.incrementTotalFailedFiles();
				throw new BusinessException(MessageFormat.format(TransformerConstants.CONST_ERR_MESSAGE_FILENOTFOUND, transformationDetailsTO.getSourceFile()));
				
			} else if(sourceFileThrowable instanceof InvalidFormatException){
				ErrorCatalog errorCatalog = new ErrorCatalog();
				errorCatalog.setFileName(transformationDetailsTO.getSourceFile().getName());
				errorCatalog.setErrorCode(TransformerConstants.CONST_ERR_CODE_INPUTFILEINVALID);
				errorCatalog.setErrorMessage(MessageFormat.format(TransformerConstants.CONST_ERR_MESSAGE_INPUTFILEINVALID, transformationDetailsTO.getSourceFile()));
				stat.setHasError(true);
				stat.setErrorCatalog(errorCatalog);
				reportManager.incrementTotalFailedFiles();
				throw new BusinessException(MessageFormat.format(TransformerConstants.CONST_ERR_MESSAGE_INPUTFILEINVALID, transformationDetailsTO.getSourceFile()));
				
			} else if(sourceFileThrowable instanceof EncryptedDocumentException){
				ErrorCatalog errorCatalog = new ErrorCatalog();
				errorCatalog.setFileName(transformationDetailsTO.getSourceFile().getName());
				errorCatalog.setErrorCode(TransformerConstants.CONST_ERR_CODE_INPUTFILEENCRYPED);
				errorCatalog.setErrorMessage(MessageFormat.format(TransformerConstants.CONST_ERR_MESSAGE_INPUTFILEENCRYPED, transformationDetailsTO.getSourceFile()));
				stat.setHasError(true);
				stat.setErrorCatalog(errorCatalog);
				reportManager.incrementTotalFailedFiles();
				throw new BusinessException(MessageFormat.format(TransformerConstants.CONST_ERR_MESSAGE_INPUTFILEENCRYPED, transformationDetailsTO.getSourceFile()));
				
			}else if(sourceFileThrowable instanceof IOException) {
				ErrorCatalog errorCatalog = new ErrorCatalog();
				errorCatalog.setFileName(transformationDetailsTO.getSourceFile().getName());
				errorCatalog.setErrorCode(TransformerConstants.CONST_ERR_CODE_UNKNOWNSYSTEMEXCEPTION);
				errorCatalog.setErrorMessage(MessageFormat.format(TransformerConstants.CONST_ERR_MESSAGE_UNKNOWNSYSTEMEXCEPTION, transformationDetailsTO.getSourceFile()));
				stat.setHasError(true);
				stat.setErrorCatalog(errorCatalog);
				reportManager.incrementTotalFailedFiles();
				throw new BusinessException(MessageFormat.format(TransformerConstants.CONST_ERR_MESSAGE_UNKNOWNSYSTEMEXCEPTION, transformationDetailsTO.getSourceFile()));
				
			}else {
				Exception sourceFileException = (Exception) sourceFileThrowable;
				ErrorCatalog errorCatalog = new ErrorCatalog();
				errorCatalog.setFileName(transformationDetailsTO.getSourceFile().getName());
				errorCatalog.setErrorCode(TransformerConstants.CONST_ERR_CODE_UNKNOWNSYSTEMEXCEPTION);
				errorCatalog.setErrorMessage(MessageFormat.format(TransformerConstants.CONST_ERR_MESSAGE_UNKNOWNSYSTEMEXCEPTION + " : " + sourceFileException.getMessage(), transformationDetailsTO.getSourceFile()));
				stat.setHasError(true);
				stat.setErrorCatalog(errorCatalog);
				reportManager.incrementTotalFailedFiles();
				throw new BusinessException(TransformerConstants.CONST_ERR_MESSAGE_UNKNOWNSYSTEMEXCEPTION + " : " + sourceFileException.getMessage());
			}
		}
		try {
			logger.info("Loading  target file: " + targetFile + "...");
			FileInputStream targetFileIS = new FileInputStream(targetFile);
			targetWorkBook = WorkbookFactory.create(targetFileIS);
			if ( targetFileIS != null ) {
				targetFileIS.close();
			}
			logger.info("Loading  target template complete");
		} catch (Throwable targetFileThrowable) {
			if (targetFileThrowable instanceof FileNotFoundException) {
				
				ErrorCatalog errorCatalog = new ErrorCatalog();
				errorCatalog.setFileName(targetFile.getName());
				errorCatalog.setErrorCode(TransformerConstants.CONST_ERR_CODE_TARGETTEMPLATENOTFOUND);
				errorCatalog.setErrorMessage(MessageFormat.format(TransformerConstants.CONST_ERR_MESSAGE_TARGETTEMPLATENOTFOUND, targetFile.getName()));
				
				reportManager.setValidationError(true);
				reportManager.setValidationFileErrorCatalog(errorCatalog);
				System.out.println("throwing exception");
				throw new BusinessException(MessageFormat.format(TransformerConstants.CONST_ERR_MESSAGE_TARGETTEMPLATENOTFOUND, targetFile));

			} else if (targetFileThrowable instanceof InvalidFormatException) {
				
				ErrorCatalog errorCatalog = new ErrorCatalog();
				errorCatalog.setFileName(targetFile.getName());
				errorCatalog.setErrorCode(TransformerConstants.CONST_ERR_CODE_TARGETTEMPLATEINVALID);
				errorCatalog.setErrorMessage(MessageFormat.format(TransformerConstants.CONST_ERR_MESSAGE_TARGETTEMPLATEINVALID, targetFile.getName()));
				
				reportManager.setValidationError(true);
				reportManager.setValidationFileErrorCatalog(errorCatalog);
				
				throw new BusinessException(MessageFormat.format(TransformerConstants.CONST_ERR_MESSAGE_TARGETTEMPLATEINVALID, targetFile));
				

			} else if (targetFileThrowable instanceof EncryptedDocumentException) {
				
				ErrorCatalog errorCatalog = new ErrorCatalog();
				errorCatalog.setFileName(targetFile.getName());
				errorCatalog.setErrorCode(TransformerConstants.CONST_ERR_CODE_TARGETTEMPLATEENCRYPTED);
				errorCatalog.setErrorMessage(MessageFormat.format(TransformerConstants.CONST_ERR_MESSAGE_TARGETTEMPLATEENCRYPTED, targetFile.getName()));
				
				reportManager.setValidationError(true);
				reportManager.setValidationFileErrorCatalog(errorCatalog);
				
				throw new BusinessException(MessageFormat.format(TransformerConstants.CONST_ERR_MESSAGE_TARGETTEMPLATEENCRYPTED, targetFile));
				

			} else if (targetFileThrowable instanceof IOException) {
				
				ErrorCatalog errorCatalog = new ErrorCatalog();
				errorCatalog.setFileName(targetFile.getName());
				errorCatalog.setErrorCode(TransformerConstants.CONST_ERR_CODE_UNKNOWNSYSTEMEXCEPTION);
				errorCatalog.setErrorMessage(MessageFormat.format(TransformerConstants.CONST_ERR_MESSAGE_UNKNOWNSYSTEMEXCEPTION, targetFile.getName()));
				
				reportManager.setValidationError(true);
				reportManager.setValidationFileErrorCatalog(errorCatalog);
				
				throw new BusinessException(MessageFormat.format(TransformerConstants.CONST_ERR_MESSAGE_UNKNOWNSYSTEMEXCEPTION, targetFile));

			} else {
				Exception targetFileException = (Exception) targetFileThrowable;
				
				ErrorCatalog errorCatalog = new ErrorCatalog();
				errorCatalog.setFileName(targetFile.getName());
				errorCatalog.setErrorCode(TransformerConstants.CONST_ERR_CODE_UNKNOWNSYSTEMEXCEPTION);
				errorCatalog.setErrorMessage(MessageFormat.format(TransformerConstants.CONST_ERR_MESSAGE_UNKNOWNSYSTEMEXCEPTION + " : " + targetFileException.getMessage(), targetFile.getName()));
				
				reportManager.setValidationError(true);
				reportManager.setValidationFileErrorCatalog(errorCatalog);
				
				throw new BusinessException(MessageFormat.format(TransformerConstants.CONST_ERR_MESSAGE_UNKNOWNSYSTEMEXCEPTION + " : " + targetFileException.getMessage(), targetFile));
			}
		}
		
		try{
			
			logger.info("Fetching source workbook data");
			
			int numOfSourceWorkbookSheet = sourceWorkbook.getNumberOfSheets();
			stat.setNumOfTabsInSource(numOfSourceWorkbookSheet);
			logger.debug("Number of Source Workbook Sheet to process [" + numOfSourceWorkbookSheet + "]");
			
			for (int i = 0; i < numOfSourceWorkbookSheet; i++) {
				Sheet sourceWorkbookSheet = sourceWorkbook.getSheetAt(i);
				if(exemptedSheetList != null && exemptedSheetList.contains(sourceWorkbookSheet.getSheetName())) {
					logger.debug("Tab [" + sourceWorkbookSheet.getSheetName() + "] is exempted from evaluation");
				} else {
					if(skipHiddenSheets && (sourceWorkbook.isSheetHidden(i) || sourceWorkbook.isSheetVeryHidden(i))) {
						logger.debug("Sheet [" + sourceWorkbook.getSheetAt(i) + "] is hidden. skipping as per configuration");
						stat.addSheetToHiddenSheetList(sourceWorkbookSheet.getSheetName());
					}else {
						if(logger.isDebugEnabled()) {
							logger.debug("Evaluating source sheet with name: [" + sourceWorkbookSheet.getSheetName() + "]");
						}
						Map<Integer, String> columnPositionMap = new HashMap<Integer, String>();
						Map<String, List<String>> columnDataMap = TransformerHelper.getColumnWiseData(sourceWorkbookSheet, columnPositionMap);
						
						logger.debug("Evaluating target workbook to insert data");
						Sheet targetSheet = targetWorkBook.getSheet(sourceWorkbookSheet.getSheetName());
						if(targetSheet == null) {
							logger.info("Sheet [" + sourceWorkbookSheet.getSheetName() + "] not found in target workbook");
							stat.addSheetToMissingList(sourceWorkbookSheet.getSheetName());
						} else {
							logger.debug("Populating target worksheet");
							TransformerHelper.setColumnWiseData(targetSheet, columnDataMap, columnPositionMap, stat);
						}
					}
				}
			}
			
			String targetFileName = ConversionUtil.getTargetFileName(sourceFile.getName(), transformationDetailsTO.isAppendVersionInOutput(), transformationDetailsTO.getTargetVersion());
			stat.setOutputFile(targetFileName);
			ConversionUtil.writeTargetFileToDisc(targetWorkBook, targetFileName, dir);
			reportManager.incrementTotalSuccessFiles();
			stat.setEndTime(new Date());
			
		}
		catch (Throwable e) {
			logger.error("Error occured! " + e.getMessage(), e);
			if ( e instanceof OutOfMemoryError ) {
				e.printStackTrace();
				System.out.println("Try running with more stack memory (eg java -Xmx1224m -jar sap-transformer.jar), you might need to close programs first.");
			}
		}
	}

}
