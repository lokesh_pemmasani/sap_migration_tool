package src.main.java.com.ericsson.cenx.saptransformer.lib.ui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultHighlighter;
import javax.swing.text.Highlighter;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;
import javax.swing.text.StyledDocument;

import org.apache.log4j.Level;

/** Creates a UI to display log messages from a SwingAppender
 * Simplified version from 
 * 
 * http://www.programcreek.com/java-api-examples/index.php?source_dir=wattzap-ce-master/src/com/omniscient/log4jcontrib/swingappender/ui/SwingAppenderUI.java
 *
 */
public class SwingAppenderUI {
	//UI attributes
	private JButton clear; //button to clear the text area
	private JButton search; //search button
	private JTextField searchField; //search field
	private JPanel buttonsPanel; //panel to hold all buttons
	public JPanel getButtonsPanel() {
		return buttonsPanel;
	}

	public void setButtonsPanel(JPanel buttonsPanel) {
		this.buttonsPanel = buttonsPanel;
	}

	public JScrollPane getScrollPane() {
		return scrollPane;
	}

	public void setScrollPane(JScrollPane scrollPane) {
		this.scrollPane = scrollPane;
	}

	private JTextPane logMessagesDisp; //display area
	private JScrollPane scrollPane;


	/* Constants */
	public static final String STYLE_REGULAR = "regular";
	public static final String STYLE_HIGHLIGHTED = "highlighted";
	public static final String STYLE_ERROR= "errorstyle";
	public static final String START = "start";
	public static final String PAUSE = "pause";
	public static final String STOP = "stop";
	public static final String CLEAR = "Clear log";
	public static final int STARTED = 0;
	public static final int PAUSED = 1;
	public static final int STOPPED = 2;

	/**
	 * An instance for SwingAppenderUI class. This holds the Singleton.
	 */
	private static SwingAppenderUI instance;

	/**
	 * Method to get an instance of the this class. This method ensures that
	 * SwingAppenderUI is a Singleton using a doule checked locking mechanism.
	 * @return An instance of SwingAppenderUI
	 */
	public static SwingAppenderUI getInstance() {
		if (instance == null) {
			synchronized(SwingAppenderUI.class) {
				if(instance == null) {
					instance = new SwingAppenderUI();
				}
			}
		}
		return instance;
	}

	/**
	 * Private constructer to ensure that this object cannot e instantiated
	 * from outside this class.
	 */
	private SwingAppenderUI() {

		//initialize buttons
		initButtonsPanel();

		//create text area to hold the log messages
		initMessageDispArea();


	}



	/**Displays the log in the text area unless dispMsg is set to false in which
	 * case it adds the log to a buffer. When dispMsg becomes true, the buffer
	 * is first flushed and it's contents are displayed in the text area.
	 * @param log The log message to be displayed in the text area
	 * @param level 
	 */
	public void doLog(String log, Level level) {
		try {
			StyledDocument sDoc = logMessagesDisp.getStyledDocument();
			Style style = sDoc.getStyle(STYLE_REGULAR);
			if ( level != null && level.equals(Level.ERROR)) {
				style = sDoc.getStyle(STYLE_ERROR);
			}
			sDoc.insertString(sDoc.getEndPosition().getOffset() -1, log, style);
			logMessagesDisp.setCaretPosition(sDoc.getEndPosition().getOffset()-1);
		} catch(BadLocationException ble) {
			System.out.println("Bad Location Exception : " + ble.getMessage());
		}


	}

	/**creates a panel to hold the buttons
	 */
	private void initButtonsPanel() {
		buttonsPanel = new JPanel();

		clear = new JButton(CLEAR);
		clear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				logMessagesDisp.setText("");
			}
		});

		searchField = new JTextField(25);
		search = new JButton("Search log");
		SearchActionListener searchActionListener = new SearchActionListener();
		search.addActionListener(searchActionListener);
		searchField.addActionListener(searchActionListener);
		
		clear.setMargin(new java.awt.Insets(1, 2, 1, 2));
		clear.setMinimumSize(new Dimension(0,0));

		search.setMargin(new java.awt.Insets(1, 2, 1, 2));
		search.setMinimumSize(new Dimension(0,0));

		searchField.setMargin(new java.awt.Insets(1, 2, 1, 2));
		searchField.setMinimumSize(new Dimension(0,0));

		buttonsPanel.add(clear);
		buttonsPanel.add(searchField);
		buttonsPanel.add(search);
		buttonsPanel.setMinimumSize(new Dimension(0,0));


	}

	public JTextPane getLogTextPane() {
		return logMessagesDisp;
	}

	/**Creates a scrollable text area
	 */
	private void initMessageDispArea() {
		logMessagesDisp = new JTextPane();

		scrollPane = new JScrollPane(logMessagesDisp);
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		//scrollPane.set
		//scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		//add styles
		StyledDocument sDoc = logMessagesDisp.getStyledDocument();
		Style def = StyleContext.getDefaultStyleContext().getStyle(StyleContext.DEFAULT_STYLE);         
		Style s1 = sDoc.addStyle(STYLE_REGULAR, def);
		StyleConstants.setFontFamily(def, "SansSerif");

		Style s2 = sDoc.addStyle(STYLE_HIGHLIGHTED, s1);
		StyleConstants.setBackground(s2, Color.BLUE);

		Style sError = sDoc.addStyle(STYLE_ERROR, s1);
		StyleConstants.setForeground(sError, Color.RED);

	}

	/**************** inner classes *************************/

	/**Accepts and responds to action events generated by the startPause
	 * button.
	 */
	class StartPauseActionListener implements ActionListener {
		/**Toggles the value of the startPause button. Also toggles
		 * the value of dispMsg.
		 * @param evt The action event
		 */
		public void actionPerformed(ActionEvent evt) {
			JButton srcButton = (JButton)evt.getSource();
			if(srcButton.getText().equals(START)) {
				srcButton.setText(PAUSE);
			}
			else if(srcButton.getText().equals(PAUSE)) {
				srcButton.setText(START);
			}
		}
	}

	class SearchActionListener implements ActionListener {

		public void actionPerformed(ActionEvent evt) {
			
			String searchTerm = searchField.getText();
			String allLogText = logMessagesDisp.getText();
			int startIndex = 0;
			int selectionIndex=-1;
			int firstHitIndex = -1;
			Highlighter hLighter = logMessagesDisp.getHighlighter();
			//clear all previous highlightes
			hLighter.removeAllHighlights();
			DefaultHighlighter.DefaultHighlightPainter highlightPainter = new DefaultHighlighter.DefaultHighlightPainter(Color.YELLOW);
			while((selectionIndex = allLogText.indexOf(searchTerm, startIndex)) != -1) {
				startIndex = selectionIndex + searchTerm.length();
				if ( firstHitIndex == -1) {
					firstHitIndex = selectionIndex;
				}
				try {
					int newLines = getNumberOfNewLinesTillSelectionIndex(allLogText, selectionIndex);
					hLighter.addHighlight(selectionIndex-newLines, (selectionIndex+searchTerm.length()-newLines), highlightPainter);
				} catch(BadLocationException ble) {
					System.out.println("Bad Location Exception: " + ble.getMessage());                                                      
				}
			}
			if ( firstHitIndex != -1) {
				logMessagesDisp.setCaretPosition(firstHitIndex);
			}
			else {
				JOptionPane.showMessageDialog(logMessagesDisp.getParent().getParent(), "Found nothing in log!","Nothing found", JOptionPane.WARNING_MESSAGE);
			}
		}

		private int getNumberOfNewLinesTillSelectionIndex(String allLogText, int selectionIndex) {
			int numberOfNewlines = 0;
			int pos = 0;
			while((pos = allLogText.indexOf("\n", pos))!=-1 && pos <= selectionIndex) {
				numberOfNewlines++;
				pos++;
			}
			return numberOfNewlines;
		}

	}

	public void close() {
		// clean up code for UI goes here. 

	}
}