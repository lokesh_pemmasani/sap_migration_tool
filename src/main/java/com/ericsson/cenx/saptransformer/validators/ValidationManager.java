package src.main.java.com.ericsson.cenx.saptransformer.validators;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.Enumeration;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import src.main.java.com.ericsson.cenx.saptransformer.TransformationDetails;
import src.main.java.com.ericsson.cenx.saptransformer.exception.BusinessException;
import src.main.java.com.ericsson.cenx.saptransformer.stats.ErrorCatalog;
import src.main.java.com.ericsson.cenx.saptransformer.stats.ReportManager;
import src.main.java.com.ericsson.cenx.saptransformer.util.PropertiesCache;
import src.main.java.com.ericsson.cenx.saptransformer.util.TransformerConstants;

public class ValidationManager {

	static Logger logger = Logger.getLogger(ValidationManager.class.getName());

	public static List<ErrorCatalog> performValidation() {

		return null;
	}
	
	public static void validateTransformer(TransformationDetails transformationDetailsTO) throws BusinessException{
		validateSource(transformationDetailsTO);
		validateOutputDirectory(transformationDetailsTO);
		validateInputOutputDirectoryForEquality(transformationDetailsTO);
		validateTargetTemplate(transformationDetailsTO);
		
	}
	
	public static void validateUploadTemplate(String templateSrcFile, String targetVersion) throws BusinessException{
		validateTemplateVersion(targetVersion);
		validateTemplateFile(templateSrcFile);
		validateAlreadyExistingFile(templateSrcFile, targetVersion);
	}
	
	public static void validateDeleteTemplate(String targetVersion) throws BusinessException{
		PropertiesCache propertiesCache = PropertiesCache.getInstance();
		String templateFileLocation = propertiesCache.getProperty(targetVersion);
		File templateFile = new File(templateFileLocation);
		if(!templateFile.exists()) {
			throw new BusinessException(MessageFormat.format(TransformerConstants.CONST_ERR_CODE_UPLOADTEMPLATEFILEALREADYDELETED + " : " + TransformerConstants.CONST_ERR_MESSAGE_UPLOADTEMPLATEFILEALREADYDELETED, templateFileLocation));
		}
	}
	
	private static void validateTemplateFile(String templateSrcFile) throws BusinessException {
		Workbook templateWorkBook = null;
		FileInputStream templateFileIS = null;
		try {

			templateFileIS = new FileInputStream(templateSrcFile);
			templateWorkBook = WorkbookFactory.create(templateFileIS);

		} catch (Throwable targetFileThrowable) {
			if (targetFileThrowable instanceof FileNotFoundException) {
				throw new BusinessException(MessageFormat.format(TransformerConstants.CONST_ERR_CODE_UPLOADTEMPLATEFILEDOESNOTEXISTS + " : " + TransformerConstants.CONST_ERR_MESSAGE_UPLOADTEMPLATEFILEDOESNOTEXISTS, new File(templateSrcFile).getName()));

			} else if (targetFileThrowable instanceof InvalidFormatException) {
				
				throw new BusinessException(MessageFormat.format(TransformerConstants.CONST_ERR_CODE_UPLOADTEMPLATEFILEINVALID + " : " + TransformerConstants.CONST_ERR_MESSAGE_UPLOADTEMPLATEFILEINVALID, new File(templateSrcFile).getName()));

			} else if (targetFileThrowable instanceof EncryptedDocumentException) {
				
				throw new BusinessException(MessageFormat.format(TransformerConstants.CONST_ERR_CODE_UPLOADTEMPLATEFILEENCRYPTED + " : " + TransformerConstants.CONST_ERR_MESSAGE_UPLOADTEMPLATEFILEENCRYPTED, new File(templateSrcFile).getName()));

			} else if (targetFileThrowable instanceof IOException) {
				
				throw new BusinessException(MessageFormat.format(TransformerConstants.CONST_ERR_CODE_UNKNOWNSYSTEMEXCEPTION + " : " + TransformerConstants.CONST_ERR_MESSAGE_UNKNOWNSYSTEMEXCEPTION, new File(templateSrcFile).getName()));

			} else {
				Exception exception = (Exception) targetFileThrowable;
				throw new BusinessException(MessageFormat.format(TransformerConstants.CONST_ERR_CODE_UNKNOWNSYSTEMEXCEPTION + " : " + TransformerConstants.CONST_ERR_MESSAGE_UNKNOWNSYSTEMEXCEPTION + " : " + exception.getMessage(), new File(templateSrcFile).getName()));
			}
		} finally {

			if ( templateFileIS != null ) {
				try {
					templateFileIS.close();
				} catch (IOException e) {
					logger.error("System exception:: " + e.getMessage());
				}
			}
			if(templateWorkBook != null) {
				try {
					templateWorkBook.close();
				} catch (IOException e) {
					logger.error("System exception:: " + e.getMessage());
				}
			}

		}
	}

	private static void validateAlreadyExistingFile(String templateSrcFile, String targetVersion) throws BusinessException {
		PropertiesCache propertiesCache = PropertiesCache.getInstance();
		String inputTemplateFileName = new File(templateSrcFile).getName();
		Enumeration<Object> keyEnum = propertiesCache.getAllKeys();
		while (keyEnum.hasMoreElements()) {
			String key = (String) keyEnum.nextElement();
			String value = new File(propertiesCache.getProperty(key)).getName();
			if(value.equals(inputTemplateFileName)) {
				throw new BusinessException(MessageFormat.format(TransformerConstants.CONST_ERR_CODE_UPLOADTEMPLATEFILEEXISTSINCONF + " : " + TransformerConstants.CONST_ERR_MESSAGE_UPLOADTEMPLATEFILEEXISTSINCONF, new File(inputTemplateFileName).getName()));
			}
		}
		
	}

	private static void validateTemplateVersion(String targetVersion) throws BusinessException {
		PropertiesCache propertiesCache = PropertiesCache.getInstance();
		if(propertiesCache.containsKey(targetVersion)) {
			throw new BusinessException(MessageFormat.format(TransformerConstants.CONST_ERR_CODE_UPLOADTEMPLATEVERSIONEXISTS + " : " + TransformerConstants.CONST_ERR_MESSAGE_UPLOADTEMPLATEVERSIONEXISTS, targetVersion));
		}
		
	}

	private static void validateSource(TransformationDetails transformationDetailsTO) throws BusinessException{
		ReportManager reportManager = ReportManager.getInstance();
		boolean bulkOperation = transformationDetailsTO.isBulkOperation();
		String sourceDir = null;
		if (bulkOperation) {
			sourceDir = transformationDetailsTO.getSourceDirectory().getAbsolutePath();
		} else {
			String absolutePath = transformationDetailsTO.getSourceFile().getAbsolutePath();
			int index = absolutePath.lastIndexOf(File.separator);
			sourceDir = absolutePath.substring(0, index);
		}

		File actualSourceDir = new File(sourceDir);
		if(!actualSourceDir.exists()) {
			
			ErrorCatalog errorCatalog = new ErrorCatalog();
			errorCatalog.setErrorCode(TransformerConstants.CONST_ERR_CODE_INPUTDIRNOTFOUND);
			errorCatalog.setErrorMessage(MessageFormat.format(TransformerConstants.CONST_ERR_MESSAGE_INPUTDIRNOTFOUND, sourceDir));
			reportManager.setValidationError(true);
			reportManager.setValidationFileErrorCatalog(errorCatalog);
			throw new BusinessException(MessageFormat.format(TransformerConstants.CONST_ERR_MESSAGE_INPUTDIRNOTFOUND, sourceDir));
			
		}
		
	}

	private static void validateOutputDirectory(TransformationDetails transformationDetailsTO) throws BusinessException{
		ReportManager reportManager = ReportManager.getInstance();
		String targetDir = transformationDetailsTO.getOutputDir().getAbsolutePath();
		
		File actualTargetDir = new File(targetDir);
		if(!actualTargetDir.exists()) {
			ErrorCatalog errorCatalog = new ErrorCatalog();
			errorCatalog.setErrorCode(TransformerConstants.CONST_ERR_CODE_OUTPUTDIRNOTFOUND);
			errorCatalog.setErrorMessage(MessageFormat.format(TransformerConstants.CONST_ERR_MESSAGE_OUTPUTDIRNOTFOUND, targetDir));
			reportManager.setValidationError(true);
			reportManager.setValidationFileErrorCatalog(errorCatalog);
			throw new BusinessException(MessageFormat.format(TransformerConstants.CONST_ERR_MESSAGE_OUTPUTDIRNOTFOUND, targetDir));
		}
	}

	private static void validateInputOutputDirectoryForEquality(TransformationDetails transformationDetailsTO) throws BusinessException{
		ReportManager reportManager = ReportManager.getInstance();
		boolean bulkOperation = transformationDetailsTO.isBulkOperation();
		String sourceDir = null;
		String targetDir = transformationDetailsTO.getOutputDir().getAbsolutePath();
		if (bulkOperation) {
			sourceDir = transformationDetailsTO.getSourceDirectory().getAbsolutePath();
		} else {
			String absolutePath = transformationDetailsTO.getSourceFile().getAbsolutePath();
			int index = absolutePath.lastIndexOf(File.separator);
			sourceDir = absolutePath.substring(0, index);
		}

		if (new File(sourceDir).equals(new File(targetDir))) {
			ErrorCatalog errorCatalog = new ErrorCatalog();
			errorCatalog.setErrorCode(TransformerConstants.CONST_ERR_CODE_INPUTOUTPUTDIREQUAL);
			errorCatalog.setErrorMessage(MessageFormat.format(TransformerConstants.CONST_ERR_MESSAGE_INPUTOUTPUTDIREQUAL, "Source Dir [" + sourceDir + "] and Target Dir [" + targetDir + "]"));
			reportManager.setValidationError(true);
			reportManager.setValidationFileErrorCatalog(errorCatalog);
			throw new BusinessException(MessageFormat.format(TransformerConstants.CONST_ERR_MESSAGE_INPUTOUTPUTDIREQUAL, "Source Dir [" + sourceDir + "] and Target Dir [" + targetDir + "]"));
		}

	}

	private static void validateTargetTemplate(TransformationDetails transformationDetailsTO) throws BusinessException {
		
		String targetFile = transformationDetailsTO.getTargetFile().getAbsolutePath();
		ReportManager reportManager = ReportManager.getInstance();
		Workbook targetWorkBook = null;
		FileInputStream targetFileIS = null;
		try {
			logger.info("Loading  target file: " + targetFile + "...");

			targetFileIS = new FileInputStream(targetFile);
			targetWorkBook = WorkbookFactory.create(targetFileIS);
			logger.info("Loading  target template complete");

		} catch (Throwable targetFileThrowable) {
			if (targetFileThrowable instanceof FileNotFoundException) {
				
				ErrorCatalog errorCatalog = new ErrorCatalog();
				errorCatalog.setFileName(new File(targetFile).getName());
				errorCatalog.setErrorCode(TransformerConstants.CONST_ERR_CODE_TARGETTEMPLATENOTFOUND);
				errorCatalog.setErrorMessage(MessageFormat.format(TransformerConstants.CONST_ERR_MESSAGE_TARGETTEMPLATENOTFOUND, new File(targetFile).getName()));
				
				reportManager.setValidationError(true);
				reportManager.setValidationFileErrorCatalog(errorCatalog);
				throw new BusinessException(MessageFormat.format(TransformerConstants.CONST_ERR_MESSAGE_TARGETTEMPLATENOTFOUND, targetFile));

			} else if (targetFileThrowable instanceof InvalidFormatException) {
				
				ErrorCatalog errorCatalog = new ErrorCatalog();
				errorCatalog.setFileName(new File(targetFile).getName());
				errorCatalog.setErrorCode(TransformerConstants.CONST_ERR_CODE_TARGETTEMPLATEINVALID);
				errorCatalog.setErrorMessage(MessageFormat.format(TransformerConstants.CONST_ERR_MESSAGE_TARGETTEMPLATEINVALID, new File(targetFile).getName()));
				
				reportManager.setValidationError(true);
				reportManager.setValidationFileErrorCatalog(errorCatalog);
				
				throw new BusinessException(MessageFormat.format(TransformerConstants.CONST_ERR_MESSAGE_TARGETTEMPLATEINVALID, targetFile));
				

			} else if (targetFileThrowable instanceof EncryptedDocumentException) {
				
				ErrorCatalog errorCatalog = new ErrorCatalog();
				errorCatalog.setFileName(new File(targetFile).getName());
				errorCatalog.setErrorCode(TransformerConstants.CONST_ERR_CODE_TARGETTEMPLATEENCRYPTED);
				errorCatalog.setErrorMessage(MessageFormat.format(TransformerConstants.CONST_ERR_MESSAGE_TARGETTEMPLATEENCRYPTED, new File(targetFile).getName()));
				
				reportManager.setValidationError(true);
				reportManager.setValidationFileErrorCatalog(errorCatalog);
				
				throw new BusinessException(MessageFormat.format(TransformerConstants.CONST_ERR_MESSAGE_TARGETTEMPLATEENCRYPTED, targetFile));
				

			} else if (targetFileThrowable instanceof IOException) {
				
				ErrorCatalog errorCatalog = new ErrorCatalog();
				errorCatalog.setFileName(new File(targetFile).getName());
				errorCatalog.setErrorCode(TransformerConstants.CONST_ERR_CODE_UNKNOWNSYSTEMEXCEPTION);
				errorCatalog.setErrorMessage(MessageFormat.format(TransformerConstants.CONST_ERR_MESSAGE_UNKNOWNSYSTEMEXCEPTION, new File(targetFile).getName()));
				
				reportManager.setValidationError(true);
				reportManager.setValidationFileErrorCatalog(errorCatalog);
				
				throw new BusinessException(MessageFormat.format(TransformerConstants.CONST_ERR_MESSAGE_UNKNOWNSYSTEMEXCEPTION, targetFile));

			} else {
				Exception targetFileException = (Exception) targetFileThrowable;
				
				ErrorCatalog errorCatalog = new ErrorCatalog();
				errorCatalog.setFileName(new File(targetFile).getName());
				errorCatalog.setErrorCode(TransformerConstants.CONST_ERR_CODE_UNKNOWNSYSTEMEXCEPTION);
				errorCatalog.setErrorMessage(MessageFormat.format(TransformerConstants.CONST_ERR_MESSAGE_UNKNOWNSYSTEMEXCEPTION + " : " + targetFileException.getMessage(), new File(targetFile).getName()));
				
				reportManager.setValidationError(true);
				reportManager.setValidationFileErrorCatalog(errorCatalog);
				
				throw new BusinessException(MessageFormat.format(TransformerConstants.CONST_ERR_MESSAGE_UNKNOWNSYSTEMEXCEPTION + " : " + targetFileException.getMessage(), targetFile));
			}
		} finally {

			if ( targetFileIS != null ) {
				try {
					targetFileIS.close();
				} catch (IOException e) {
					logger.error("System exception:: " + e.getMessage());
				}
			}
			if(targetWorkBook != null) {
				try {
					targetWorkBook.close();
				} catch (IOException e) {
					logger.error("System exception:: " + e.getMessage());
				}
			}

		}

	}

	
	

}
