package src.main.java.com.ericsson.cenx.saptransformer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;

import src.main.java.com.ericsson.cenx.saptransformer.stats.TransformerStatisticalData;

public class TransformerHelper {
	
	static Logger logger = Logger.getLogger(TransformerHelper.class.getName());
	
	public static Map<String, List<String>> getColumnWiseData(Sheet inputSheet, Map<Integer, String> columnPositionMap) {
		
		if (logger.isDebugEnabled()) {
			logger.debug("Fetching columnwise data for sheet " + inputSheet.getSheetName());
		}
		Map<String, List<String>> columnDataMap = new LinkedHashMap<String, List<String>>();

		DataFormatter dataFormatter = new DataFormatter();

		for (Row row : inputSheet) {
			if (row.getRowNum() == 0) {
				if (logger.isDebugEnabled()) {
					logger.debug("Evaluating columns for sheet :: [" + inputSheet.getSheetName() + "]");
				}
				for (Cell cell : row) {
					String columnName = cell.getRichStringCellValue().getString();
					columnDataMap.put(columnName, new ArrayList<String>());
					Integer columnIndex = cell.getColumnIndex();
					columnPositionMap.put(columnIndex, columnName);
				}
				if (logger.isDebugEnabled()) {
				logger.debug("Available Columns are:: [" + columnPositionMap.values() + "]");
				}
				
			} else {
				for (Cell cell : row) {
					String cellValue = dataFormatter.formatCellValue(cell);
					if (cell.getCellType() == Cell.CELL_TYPE_FORMULA) {

						cellValue = "FOR:" + cell.getCellFormula();
					} else {
						cellValue = "STR:" + cellValue;
					}
					List<String> columnData = columnDataMap.get(columnPositionMap.get(cell.getColumnIndex()));
					if (columnData == null) {
						columnData = new ArrayList<String>();
						columnDataMap.put(columnPositionMap.get(cell.getColumnIndex()), columnData);
					}
					columnData.add(cellValue);
				}

			}
		}
		return columnDataMap;
	}

	public static void setColumnWiseData(Sheet targetSheet, Map<String, List<String>> sourceColumnDataMap,
			Map<Integer, String> sourceColumnPositionMap, TransformerStatisticalData stat) {
		
		Map<String, Integer> targetColumnPositionMap = new HashMap<String, Integer>();

		for (Row row : targetSheet) {
			if (row.getRowNum() == 0) {
				for (Cell cell : row) {
					String columnName = cell.getRichStringCellValue().getString();
					Integer columnIndex = cell.getColumnIndex();
					targetColumnPositionMap.put(columnName, columnIndex);
				}
			}
		}

		for (Entry<Integer, String> sourceColumnPositionEntry : sourceColumnPositionMap.entrySet()) {

			String sourceColumn = sourceColumnPositionEntry.getValue();
			if (targetColumnPositionMap.containsKey(sourceColumn)) {
				int targetColumnIndex = targetColumnPositionMap.get(sourceColumn);
				List<String> sourceValueList = sourceColumnDataMap.get(sourceColumn);
				int sourceValueIndex = 1;
				for (String sourceValue : sourceValueList) {
					int sourceValueType = Cell.CELL_TYPE_STRING;
					if (sourceValue != null && !sourceValue.isEmpty()) {
						String[] sourceValArray = sourceValue.split(":", 2);
						if (sourceValArray.length >= 2) {
							if (sourceValArray[0].equals("STR")) {
								sourceValue = sourceValArray[1];

							} else if (sourceValArray[0].equals("FOR")) {
								sourceValueType = Cell.CELL_TYPE_FORMULA;
								sourceValue = sourceValArray[1];
							}
						}
					}
					Row targetRow = targetSheet.getRow(sourceValueIndex);
					if (targetRow == null) {
						targetRow = targetSheet.createRow(sourceValueIndex);
					}
					Cell targetCell = targetRow.getCell(targetColumnIndex);
					if (targetCell == null) {
						targetCell = targetRow.createCell(targetColumnIndex);
					}
					if ((sourceValue != null || !sourceValue.isEmpty()) && sourceValue.length() > 0) {
						if (sourceValueType == Cell.CELL_TYPE_FORMULA) {
							targetCell.setCellFormula(sourceValue);
						} else {
							targetCell.setCellValue(sourceValue);
						}
					}
					sourceValueIndex++;
				}

			} else {
				logger.info("Source Column:: [" + sourceColumn + "] not found in target");
				stat.addColumnToMissingList(targetSheet.getSheetName(), sourceColumn);
			}

		}
	}

}
